const React = require('react');

const Immutable = require('immutable');

const ReactDOM = require('react-dom');
const { Route, BrowserRouter: Router } = require('react-router-dom');
const { Provider } = require('react-redux');
const { createBrowserHistory } = require('history');
const document = require('global/document');

const MasterPage = require('checkout/main/connectors/masterPageConnector');

const mainModule = require('checkout/main/module');
const rootReducer = require('checkout/main/reducers/rootReducer');
const initialState = require('checkout/main/constants/initialState');
const storeFactory = require('checkout/main/helpers/storeFactory');

require('checkout/styles/index.less');
require('checkout/styles/components/index.less');

mainModule.run();

const immutableInitialState = Immutable.fromJS(initialState);
const store = storeFactory.create(immutableInitialState, rootReducer);
const rootElement = document.getElementById('main');

ReactDOM.render(
	<Provider store={store}>
		<Router history={createBrowserHistory()}>
			<Route component={MasterPage} />
		</Router>
	</Provider>,
	rootElement
);

