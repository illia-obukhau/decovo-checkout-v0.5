const { combineReducers } = require('redux-immutable');

const orderReducer = require('checkout/state/reducers/order');
const shoppingBagReducer = require('checkout/state/reducers/shoppingBag');
const vendorConfigReducer = require('checkout/state/reducers/vendorConfig');
const notifsReducer = require('checkout/features/notifications/notificationReducer');
const formReducer = require('redux-form').reducer;

const rootReducer = combineReducers({
	orderState: orderReducer,
	vendorConfigState: vendorConfigReducer,
	shoppingBagState: shoppingBagReducer,
	notifs: notifsReducer,
	form: formReducer
});

module.exports = rootReducer;
