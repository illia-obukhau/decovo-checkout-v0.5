const { createStore, compose, applyMiddleware } = require('redux');
const { composeWithDevTools } = require('redux-devtools-extension');
const thunkMiddleware = require('redux-thunk').default;
const promiseMiddleware = require('redux-promise-middleware').default;
const notificationMiddleware = require('checkout/features/notifications/notificationMiddleware');
const mapPayloadToStateModelMiddleware = require('checkout/state/middlewares/mapPayloadToStateModelMiddleware');

const middlewares = [
	promiseMiddleware({
		promiseTypeSuffixes: ['PENDING', 'SUCCESS', 'ERROR']
	}),
	thunkMiddleware,
	mapPayloadToStateModelMiddleware,
	notificationMiddleware
];

if (DEBUG) { // eslint-disable-line no-undef
	const loggerMiddlewareFactory = require('redux-logger').createLogger; // eslint-disable-line global-require
	const loggerMiddleware = loggerMiddlewareFactory({
		collapsed: true,
		stateTransformer: state => state && state.toJSON()
	});

	middlewares.push(loggerMiddleware);
}

const enhancer = DEBUG ? // eslint-disable-line no-undef
composeWithDevTools(applyMiddleware(...middlewares)) :
	compose(applyMiddleware(...middlewares));

module.exports.create = function (initialState, rootReducer) {
	return createStore(rootReducer, initialState, enhancer);
};
