const React = require('react');

const { PropTypes } = require('prop-types');

require('./footer-navigation.less');


function FooterNavigation(props) {
	const linkContainers = props.navigationItems.map((item, index) => {
		return (
			<div className="footer-navigation__item-container" key={`footer-navigation__item-${index}`}>
				<a href={item.href} className="footer-navigation__item">{item.text}</a>
			</div>
		);
	});

	return (
		<div className="footer-navigation">
			<div className="footer-navigation__inner-container">
				{linkContainers}
			</div>
		</div>
	);
}

FooterNavigation.propTypes = {
	navigationItems: PropTypes.arrayOf(PropTypes.object).isRequired
};


module.exports = FooterNavigation;

