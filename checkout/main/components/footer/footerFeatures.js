const React = require('react');

const { PropTypes } = require('prop-types');

require('./footer-features.less');


function FooterFeatures(props) {
	return (
		<div className="footer-features">
			<div className="footer-features__inner-container">
				{
					props.features.map((feature, index) => {
						return (
							<div className="footer-features__item-container" key={`feature-${index}`}>
								<span className="footer-features__item">{feature}</span>
							</div>
						);
					})
				}
			</div>
		</div>
	);
}

FooterFeatures.propTypes = {
	features: PropTypes.arrayOf(PropTypes.string).isRequired
};

module.exports = FooterFeatures;

