const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');
const resources = require('checkout/common/constants/resources');

const Copyright = require('checkout/main/components/copyright');

require('./footer.less');

const FooterNavigation = require('./footerNavigation');
const FooterFeatures = require('./footerFeatures');

class Footer extends Component {
	shouldComponentUpdate() {
		return false;
	}

	render() {
		return (
			<footer className="footer">
				<div className="footer__inner-container layout__container">
					<div className="footer__info-container">
						<div className="footer__info" dangerouslySetInnerHTML={{ __html: this.props.customerInfo }} />
					</div>

					<div className="footer__navigation-container">
						<FooterNavigation navigationItems={this.props.links} />
					</div>

					<div className="footer__copyright-container">
						<Copyright poweredBy={this.props.copyrightOwner} details={this.props.copyrightDetails} />
					</div>
				</div>

				<div className="footer__features-container layout__container">
					<FooterFeatures features={resources['footer.features']} />
				</div>
			</footer>
		);
	}
}

Footer.propTypes = {
	links: PropTypes.arrayOf(PropTypes.object).isRequired,
	customerInfo: PropTypes.string.isRequired,
	copyrightOwner: PropTypes.string.isRequired,
	copyrightDetails: PropTypes.string.isRequired
};

module.exports = Footer;

