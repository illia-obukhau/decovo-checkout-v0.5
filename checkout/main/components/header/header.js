const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');

const resources = require('checkout/common/constants/resources');

require('./header.less');
const defaultLogoImage = require('checkout/static/images/footer-logo.svg');

const PrimaryLogo = require('checkout/main/components/primaryLogo');

class Header extends Component {
	shouldComponentUpdate() {
		return false;
	}

	renderLogoContainer(imgSrc) {
		let logoContainer = '';
		if (imgSrc) {
			logoContainer = (
				<div className="header__logo-container">
					<PrimaryLogo imgSrc={imgSrc} />
				</div>
			);
		}

		return logoContainer;
	}

	renderTitleContainer(title) {
		let titleContainer = '';
		if (title) {
			titleContainer = (
				<div className="header__title-container">
					<h2 className="header__title">{title}</h2>
				</div>
			);
		}

		return titleContainer;
	}

	render() {
		const logoContainer = this.renderLogoContainer(this.props.imgSrc);
		const titleContainer = this.renderTitleContainer(this.props.title);

		return (
			<header className="header">
				<div className="header__inner-container layout__container">
					<a className="header__main-link" href="/">
						{logoContainer}
						{titleContainer}
					</a>
				</div>
			</header>
		);
	}
}

Header.propTypes = {
	imgSrc: PropTypes.string,
	title: PropTypes.string
};

Header.defaultProps = {
	imgSrc: defaultLogoImage,
	title: resources['header.title']
};

module.exports = Header;

