const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');

require('./primaryLogo.less');

class PrimaryLogo extends Component {
	render() {
		return (
			<div className="primary-logo">
				<div className="primary-logo__image-container">
					<img src={this.props.imgSrc} className="primary-logo__image" alt="logo" />
				</div>
			</div>
		);
	}
}

PrimaryLogo.propTypes = {
	imgSrc: PropTypes.string.isRequired
};

module.exports = PrimaryLogo;

