const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');
const resources = require('checkout/common/constants/resources');

require('./copyright.less');

class Copyright extends Component {
	render() {
		return (
			<div className="copyright">
				<div className="copyright__inner-container">
					<div className="copyright__powered-by-container">
						<div className="copyright__powered-by">{resources['copyright.poweredBy']}</div>
						<div className="copyright__dynamic-info" dangerouslySetInnerHTML={{ __html: this.props.poweredBy }} />
					</div>

					<div className="copyright__details-container">
						<div className="copyright__details" dangerouslySetInnerHTML={{ __html: this.props.details }} />
					</div>
				</div>
			</div>
		);
	}
}

Copyright.propTypes = {
	poweredBy: PropTypes.string.isRequired,
	details: PropTypes.string.isRequired
};

module.exports = Copyright;

