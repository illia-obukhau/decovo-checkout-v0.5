const BaseConnector = require('checkout/main/connectors/baseConnector');
const NotFoundPage = require('checkout/main/pages/notFoundPage');

class NotFoundPageConnector extends BaseConnector {
	mapStateToProps(state) {
		return { };
	}

	mapDispatchToProps(dispatch) {
		return {

		};
	}
}

module.exports = (new NotFoundPageConnector()).connect(NotFoundPage);
