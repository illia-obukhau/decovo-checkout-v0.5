const BaseConnector = require('checkout/main/connectors/baseConnector');
const CheckoutPage = require('checkout/main/pages/checkoutPage');

const applicationActions = require('checkout/state/actions/applicationActions');
const { initialize, submit, change, untouch } = require('redux-form');

class CheckoutPageConnector extends BaseConnector {
	mapStateToProps(state) {
		const order = this.getOrder(state);
		const vendorConfig = this.getVendorConfig(state);
		const shoppingBag = this.getShoppingBag(state);
		const isBusy = this.isBusy(state, ['orderState']);

		return { order, vendorConfig, shoppingBag, isBusy };
	}

	mapDispatchToProps(dispatch) {
		const boundApplicationActions = this.bindActionCreators(applicationActions, dispatch);
		const boundReduxFormActions = this.bindActionCreators({ initialize, submit, change, untouch }, dispatch);

		return {
			onOrderSubmit: boundApplicationActions.onOrderSubmit,
			onCheckoutLoaded: boundApplicationActions.onCheckoutLoaded,
			onPromoCodeApply: boundApplicationActions.onPromoCodeApply,
			onShoppingBagChange: boundApplicationActions.onShoppingBagChange,
			initializeReduxForm: boundReduxFormActions.initialize,
			submitReduxForm: boundReduxFormActions.submit,
			changeReduxFormField: boundReduxFormActions.change,
			untouchReduxFormField: boundReduxFormActions.untouch
		};
	}
}

module.exports = (new CheckoutPageConnector()).connect(CheckoutPage);
