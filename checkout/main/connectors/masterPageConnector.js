const notificationActions = require('redux-notifications').actions;
const applicationActions = require('checkout/state/actions/applicationActions');

const BaseConnector = require('checkout/main/connectors/baseConnector');
const MasterPage = require('checkout/main/pages/masterPage');

class MasterPageConnector extends BaseConnector {
	mapStateToProps(state) {
		const vendorConfig = this.getVendorConfig(state);

		return { vendorConfig };
	}

	mapDispatchToProps(dispatch) {
		// Here can be connected some common logic like notifications, modals etc.
		const boundNotificationActions = this.bindActionCreators(notificationActions, dispatch);
		const boundApplicationActions = this.bindActionCreators(applicationActions, dispatch);

		return {
			dismissNotification: boundNotificationActions.notifClear,
			onAppLoaded: boundApplicationActions.onAppLoaded
		};
	}
}

module.exports = (new MasterPageConnector()).connect(MasterPage);
