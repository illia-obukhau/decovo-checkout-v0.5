const { bindActionCreators } = require('redux');
const { connect } = require('react-redux');
const { withRouter } = require('react-router-dom');

const autoMapper = require('checkout/common/helpers/autoMapper');
const OrderModel = require('checkout/domain/models/orderModel');
const ShoppingBagModel = require('checkout/domain/models/shoppingBagModel');
const VendorConfigModel = require('checkout/domain/models/vendorConfigModel');

class BaseConnector {
	getOrder(state) {
		const orderState = state.get('orderState').toJS();

		return autoMapper.map('OrderStateModel', OrderModel, orderState.model);
	}

	getShoppingBag(state) {
		const shoppingBagState = state.get('shoppingBagState').toJS();

		return autoMapper.map('ShoppingBagStateModel', ShoppingBagModel, shoppingBagState.model);
	}

	getVendorConfig(state) {
		const vendorConfigState = state.get('vendorConfigState').toJS();

		return autoMapper.map('VendorConfigStateModel', VendorConfigModel, vendorConfigState.model);
	}

	isBusy(state, statesNames) {
		for (let index = 0; index < statesNames.length; index++) {
			const stateName = statesNames[index];
			const immutableCurrentState = state.get(stateName);
			const currentState = immutableCurrentState && immutableCurrentState.toJS();

			if (currentState && currentState.isBusy) {
				return true;
			}
		}

		return false;
	}

	mapStateToProps() {
		return {};
	}

	mapDispatchToProps() {
		return {};
	}

	bindActionCreators(...args) {
		return bindActionCreators(...args);
	}

	connect(Component, isRouter = true) {
		if (isRouter) {
			return connect(
				this.mapStateToProps.bind(this),
				this.mapDispatchToProps.bind(this)
			)(withRouter(Component));
		}

		return connect(
			this.mapStateToProps.bind(this),
			this.mapDispatchToProps.bind(this)
		)(Component);
	}
}

module.exports = BaseConnector;
