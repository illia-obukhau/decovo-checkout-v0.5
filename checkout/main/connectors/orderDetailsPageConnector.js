const BaseConnector = require('checkout/main/connectors/baseConnector');
const OrderDetailsPage = require('checkout/main/pages/orderDetailsPage');
const locationActions = require('checkout/state/actions/locationActions');
const applicationActions = require('checkout/state/actions/applicationActions');

class OrderDetailsPageConnector extends BaseConnector {
	mapStateToProps(state) {
		const order = this.getOrder(state);
		const vendorConfig = this.getVendorConfig(state);

		return {
			order,
			vendorConfig
		};
	}

	mapDispatchToProps(dispatch) {
		const boundLocationActions = this.bindActionCreators(locationActions, dispatch);
		const boundApplicationActions = this.bindActionCreators(applicationActions, dispatch);

		return {
			onGetCoordinatesByAddress: boundLocationActions.getCoordinatesByAddress,
			onOrderLoaded: boundApplicationActions.onOrderLoaded
		};
	}
}

module.exports = (new OrderDetailsPageConnector()).connect(OrderDetailsPage);
