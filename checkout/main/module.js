const ajax = require('common/helpers/ajax');
const Module = require('common/helpers/module');

const domainModule = require('checkout/domain/module');
const stateModule = require('checkout/state/module');
const commonModule = require('checkout/common/module');
const errorInterceptor = require('common/httpInterceptors/errorInterceptor');

const mainModule = new Module(stateModule, domainModule, commonModule);

mainModule.configure(() => {
	ajax.addMiddlewares([errorInterceptor]);
});

module.exports = mainModule;
