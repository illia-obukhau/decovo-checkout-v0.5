const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');
const routes = require('checkout/main/constants/routes');

const OrderModel = require('checkout/domain/models/orderModel');
const ShoppingBagModel = require('checkout/domain/models/shoppingBagModel');
const VendorConfigModel = require('checkout/domain/models/vendorConfigModel');
const resources = require('checkout/common/constants/resources');
const Page = require('common/components/page');
const OrderEditor = require('checkout/domain/components/orderEditor');

class CheckoutPage extends Component {
	constructor(props, context, updater) {
		super(props, context, updater);

		this.onOrderSubmit = this.onOrderSubmit.bind(this);
	}

	componentDidMount() {
		const { order, match: { params } } = this.props;

		if (!order && params.id) {
			this.props.onCheckoutLoaded(params.id)
				.catch(() => {
					this.props.history.push(routes.checkout.notFound);
				});
		} else if (!order) {
			this.props.history.push(routes.checkout.notFound);
		}
	}

	onOrderSubmit(newOrder) {
		return this.props.onOrderSubmit(newOrder)
			.then((result) => {
				const nextPath = routes.checkout.order.replace(/:id/, result.value.id);

				this.props.history.push(nextPath);
			});
	}

	render() {
		const {
			order,
			shoppingBag,
			initializeReduxForm,
			submitReduxForm,
			changeReduxFormField,
			untouchReduxFormField,
			vendorConfig,
			onShoppingBagChange,
			onPromoCodeApply
		} = this.props;

		return (
			<Page title={resources['checkoutEditor.title']} isLoaded={!!order && !!shoppingBag}>
				<OrderEditor
					model={order}
					shoppingBag={shoppingBag}
					vendorConfig={vendorConfig}
					onOrderSubmit={this.onOrderSubmit}
					onPromoCodeApply={onPromoCodeApply}
					onShoppingBagChange={onShoppingBagChange}
					initializeReduxForm={initializeReduxForm}
					submitReduxForm={submitReduxForm}
					changeReduxFormField={changeReduxFormField}
					untouchReduxFormField={untouchReduxFormField}
				/>
			</Page>
		);
	}
}

CheckoutPage.propTypes = {
	match: PropTypes.object.isRequired,
	history: PropTypes.object.isRequired,
	order: PropTypes.instanceOf(OrderModel),
	shoppingBag: PropTypes.instanceOf(ShoppingBagModel),
	vendorConfig: PropTypes.instanceOf(VendorConfigModel),
	onOrderSubmit: PropTypes.func.isRequired,
	onCheckoutLoaded: PropTypes.func.isRequired,
	onPromoCodeApply: PropTypes.func.isRequired,
	onShoppingBagChange: PropTypes.func.isRequired,
	initializeReduxForm: PropTypes.func.isRequired,
	submitReduxForm: PropTypes.func.isRequired,
	changeReduxFormField: PropTypes.func.isRequired,
	untouchReduxFormField: PropTypes.func.isRequired
};

module.exports = CheckoutPage;
