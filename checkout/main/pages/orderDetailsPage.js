const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');

const routes = require('checkout/main/constants/routes');
const templateHelper = require('common/helpers/templateHelper');
const OrderModel = require('checkout/domain/models/orderModel');
const VendorConfigModel = require('checkout/domain/models/vendorConfigModel');

const resources = require('checkout/common/constants/resources');
const Page = require('common/components/page');
const OrderDetails = require('checkout/domain/components/orderDetails');

class orderDetailsPage extends Component {
	constructor(props, context, updater) {
		super(props, context, updater);

		this.onContinueShoppingClick = this.onContinueShoppingClick.bind(this);
	}

	componentDidMount() {
		const { order, match: { params } } = this.props;

		if (!order && params.id) {
			this.props.onOrderLoaded(params.id)
				.catch(() => {
					this.props.history.push(routes.checkout.notFound);
				});
		} else if (!order) {
			this.props.history.push(routes.checkout.notFound);
		}
	}

	onContinueShoppingClick() {
		const nextPath = routes.checkout.checkout.replace(/:id/, this.props.order.id);

		this.props.history.push(nextPath);
	}

	isPageFullyLoaded() {
		const { order, vendorConfig } = this.props;

		return !!order && !!vendorConfig;
	}

	render() {
		const { order } = this.props;
		const titleMessage = order ? templateHelper.format(resources['orderDetails.title'], { orderId: order.id }) : null;

		return (
			<Page title={titleMessage} isLoaded={!!order}>
				<OrderDetails
					config={this.props.vendorConfig}
					onContinueShoppingClick={this.onContinueShoppingClick}
					model={order}
					onGetCoordinatesByAddress={this.props.onGetCoordinatesByAddress}
				/>
			</Page>
		);
	}
}

orderDetailsPage.propTypes = {
	order: PropTypes.instanceOf(OrderModel),
	vendorConfig: PropTypes.instanceOf(VendorConfigModel),
	match: PropTypes.object.isRequired,
	history: PropTypes.object.isRequired,
	onGetCoordinatesByAddress: PropTypes.func.isRequired,
	onOrderLoaded: PropTypes.func.isRequired
};

module.exports = orderDetailsPage;
