const React = require('react');

const { Component } = React;
const Page = require('common/components/page');

class CheckoutPage extends Component {
	render() {
		return (
			<Page title="404 not found!">
				Resource not found!
			</Page>
		);
	}
}

module.exports = CheckoutPage;
