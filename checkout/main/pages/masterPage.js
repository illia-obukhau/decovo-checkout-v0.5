const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');

const { Switch, Route } = require('react-router-dom');
const routes = require('checkout/main/constants/routes');

const Header = require('checkout/main/components/header');
const Footer = require('checkout/main/components/footer');
const GlobalToaster = require('checkout/features/notifications/globalToaster');

const CheckoutPage = require('checkout/main/connectors/checkoutPageConnector');
const OrderDetailsPage = require('checkout/main/connectors/orderDetailsPageConnector');
const NotFoundPage = require('checkout/main/connectors/notFoundPageConnector');
const Spinner = require('common/components/spinner');

const VendorConfigModel = require('checkout/domain/models/vendorConfigModel');

const defaultCopyrightOwnerImgSrc = require('checkout/static/images/footer-logo.svg');

class MasterPage extends Component {
	componentDidMount() {
		this.props.onAppLoaded();
	}

	render() {
		const { dismissNotification, vendorConfig } = this.props;
		const customerInfo = '<p>Contact <a href="#">Customer Support</a> We’re here to help!</p><br/><p>Monday - Friday, 7am - 7pm EST</p>';
		const copyrightOwner = `<a href="#"><img src="${defaultCopyrightOwnerImgSrc}" alt="DECOVO"></a>`;
		const copyrightDetails = '<p>&copy; 2017 <a href="#">Decovo, Inc.</a>  All Rights Reserved</p>';
		const links = [{ href: '#main', text: 'FAQ' }, { href: '#main', text: 'Shipping & Returns' }];

		return (
			<div className="application layout">
				<Spinner isLoaded={!!vendorConfig}>
					<Header />

					<Switch>
						<Route exact={true} path={routes.checkout.checkout} component={CheckoutPage} />
						<Route path={routes.checkout.order} component={OrderDetailsPage} />
						<Route component={NotFoundPage} />
					</Switch>

					<Footer
						customerInfo={customerInfo}
						copyrightOwner={copyrightOwner}
						copyrightDetails={copyrightDetails}
						links={links}
					/>
					<GlobalToaster onClick={dismissNotification} />
				</Spinner>
			</div>
		);
	}
}

MasterPage.propTypes = {
	dismissNotification: PropTypes.func.isRequired,
	vendorConfig: PropTypes.instanceOf(VendorConfigModel),
	onAppLoaded: PropTypes.func.isRequired
};

module.exports = MasterPage;
