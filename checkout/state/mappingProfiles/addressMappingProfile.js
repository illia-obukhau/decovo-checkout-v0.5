const autoMapper = require('checkout/common/helpers/autoMapper');
const AddressModel = require('checkout/domain/models/addressModel');

autoMapper.createMap('AddressStateModel', AddressModel)
	.forAllMembers(autoMapper.copyOwnProperties);

autoMapper.createMap(AddressModel, 'AddressStateModel')
	.forAllMembers(autoMapper.copyDefinedProperties);

