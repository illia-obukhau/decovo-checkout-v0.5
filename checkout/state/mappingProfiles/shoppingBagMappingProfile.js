const autoMapper = require('checkout/common/helpers/autoMapper');
const ShoppingBagModel = require('checkout/domain/models/shoppingBagModel');
const ShoppingBagItemModel = require('checkout/domain/models/shoppingBagItemModel');

autoMapper.createMap('ShoppingBagStateModel', ShoppingBagModel)
	.forAllMembers(autoMapper.copyOwnProperties)
	.forMember('items', opts => autoMapper.map('ShoppingBagItemStateModel', ShoppingBagItemModel, opts.sourceObject.items));

autoMapper.createMap(ShoppingBagModel, 'ShoppingBagStateModel')
	.forAllMembers(autoMapper.copyDefinedProperties)
	.forMember('items', opts => autoMapper.map(ShoppingBagItemModel, 'ShoppingBagItemStateModel', opts.sourceObject.items));

