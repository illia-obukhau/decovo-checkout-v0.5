const autoMapper = require('checkout/common/helpers/autoMapper');
const CardInfoModel = require('checkout/domain/models/cardInfoModel');

autoMapper.createMap('CardInfoStateModel', CardInfoModel)
	.forAllMembers(autoMapper.copyOwnProperties);

autoMapper.createMap(CardInfoModel, 'CardInfoStateModel')
	.forAllMembers(autoMapper.copyDefinedProperties);
