const autoMapper = require('checkout/common/helpers/autoMapper');
const ContactInfoModel = require('checkout/domain/models/contactInfoModel');
const AddressModel = require('checkout/domain/models/addressModel');

autoMapper.createMap('ContactInfoStateModel', ContactInfoModel)
	.forAllMembers(autoMapper.copyOwnProperties)
	.forMember('shippingAddress', opts => autoMapper.map('AddressStateModel', AddressModel, opts.sourceObject.shippingAddress));

autoMapper.createMap(ContactInfoModel, 'ContactInfoStateModel')
	.forAllMembers(autoMapper.copyDefinedProperties)
	.forMember('shippingAddress', opts => autoMapper.map(AddressModel, 'AddressStateModel', opts.sourceObject.shippingAddress));

