const autoMapper = require('checkout/common/helpers/autoMapper');
const AddressModel = require('checkout/domain/models/addressModel');
const CardInfoModel = require('checkout/domain/models/cardInfoModel');
const PaymentInfoModel = require('checkout/domain/models/paymentInfoModel');

autoMapper.createMap('PaymentInfoStateModel', PaymentInfoModel)
	.forAllMembers(autoMapper.copyOwnProperties)
	.forMember('billingAddress', opts => autoMapper.map('AddressStateModel', AddressModel, opts.sourceObject.billingAddress))
	.forMember('cardInfo', opts => autoMapper.map('CardInfoStateModel', CardInfoModel, opts.sourceObject.cardInfo));

autoMapper.createMap(PaymentInfoModel, 'PaymentInfoStateModel')
	.forAllMembers(autoMapper.copyDefinedProperties)
	.forMember('billingAddress', opts => autoMapper.map(AddressModel, 'AddressStateModel', opts.sourceObject.billingAddress))
	.forMember('cardInfo', opts => autoMapper.map(CardInfoModel, 'CardInfoStateModel', opts.sourceObject.cardInfo));
