const autoMapper = require('checkout/common/helpers/autoMapper');
const DiscountModel = require('checkout/domain/models/discountModel');

autoMapper.createMap('DiscountStateModel', DiscountModel)
	.forAllMembers(autoMapper.copyOwnProperties);

autoMapper.createMap(DiscountModel, 'DiscountStateModel')
	.forAllMembers(autoMapper.copyDefinedProperties);

