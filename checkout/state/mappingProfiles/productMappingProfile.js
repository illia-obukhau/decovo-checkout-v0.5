const autoMapper = require('checkout/common/helpers/autoMapper');
const ProductModel = require('checkout/domain/models/productModel');

autoMapper.createMap('ProductStateModel', ProductModel)
	.forAllMembers(autoMapper.copyOwnProperties);

autoMapper.createMap(ProductModel, 'ProductStateModel')
	.forAllMembers(autoMapper.copyDefinedProperties);

