const autoMapper = require('checkout/common/helpers/autoMapper');
const OrderModel = require('checkout/domain/models/orderModel');
const ShoppingBagModel = require('checkout/domain/models/shoppingBagModel');
const ContactInfoModel = require('checkout/domain/models/contactInfoModel');
const PaymentInfoModel = require('checkout/domain/models/paymentInfoModel');

autoMapper.createMap('OrderStateModel', OrderModel)
	.forAllMembers(autoMapper.copyOwnProperties)
	.forMember('contactInfo', opts => autoMapper.map('ContactInfoStateModel', ContactInfoModel, opts.sourceObject.contactInfo))
	.forMember('shoppingBag', opts => autoMapper.map('ShoppingBagStateModel', ShoppingBagModel, opts.sourceObject.shoppingBag))
	.forMember('paymentInfo', opts => autoMapper.map('PaymentInfoStateModel', PaymentInfoModel, opts.sourceObject.paymentInfo));

autoMapper.createMap(OrderModel, 'OrderStateModel')
	.forAllMembers(autoMapper.copyDefinedProperties)
	.forMember('contactInfo', opts => autoMapper.map(ContactInfoModel, 'ContactInfoStateModel', opts.sourceObject.contactInfo))
	.forMember('shoppingBag', opts => autoMapper.map(ShoppingBagModel, 'ShoppingBagStateModel', opts.sourceObject.shoppingBag))
	.forMember('paymentInfo', opts => autoMapper.map(PaymentInfoModel, 'PaymentInfoStateModel', opts.sourceObject.paymentInfo));

