const autoMapper = require('checkout/common/helpers/autoMapper');
const ShoppingBagItemModel = require('checkout/domain/models/shoppingBagItemModel');
const ProductModel = require('checkout/domain/models/productModel');

autoMapper.createMap('ShoppingBagItemStateModel', ShoppingBagItemModel)
	.forAllMembers(autoMapper.copyOwnProperties)
	.forMember('product', opts => autoMapper.map('ProductStateModel', ProductModel, opts.sourceObject.product));

autoMapper.createMap(ShoppingBagItemModel, 'ShoppingBagItemStateModel')
	.forAllMembers(autoMapper.copyDefinedProperties)
	.forMember('product', opts => autoMapper.map(ProductModel, 'ProductStateModel', opts.sourceObject.product));

