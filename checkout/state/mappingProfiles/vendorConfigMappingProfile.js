const autoMapper = require('checkout/common/helpers/autoMapper');
const VendorConfigModel = require('checkout/domain/models/vendorConfigModel');
const ContactConfigModel = require('checkout/domain/models/contactConfigModel');
const ShoppingBagConfigModel = require('checkout/domain/models/shoppingBagConfigModel');
const PaymentConfigModel = require('checkout/domain/models/paymentConfigModel');
const AddressConfigModel = require('checkout/domain/models/addressConfigModel');
const StatesConfigModel = require('checkout/domain/models/statesConfigModel');

autoMapper.createMap('VendorConfigStateModel', VendorConfigModel)
	.forAllMembers(autoMapper.copyOwnProperties)
	.forMember('contactConfig', opts => autoMapper.map('ContactConfigStateModel', ContactConfigModel, opts.sourceObject.contactConfig))
	.forMember('shoppingBagConfig',
		opts => autoMapper.map('ShoppingBagConfigStateModel', ShoppingBagConfigModel, opts.sourceObject.shoppingBagConfig)
	)
	.forMember('paymentConfig', opts => autoMapper.map('PaymentConfigStateModel', PaymentConfigModel, opts.sourceObject.paymentConfig));

autoMapper.createMap(VendorConfigModel, 'VendorConfigStateModel')
	.forAllMembers(autoMapper.copyDefinedProperties)
	.forMember('contactConfig', opts => autoMapper.map(ContactConfigModel, 'ContactConfigStateModel', opts.sourceObject.contactConfig))
	.forMember('shoppingBagConfig',
		opts => autoMapper.map(ShoppingBagConfigModel, 'ShoppingBagConfigStateModel', opts.sourceObject.shoppingBagConfig)
	)
	.forMember('paymentConfig', opts => autoMapper.map(PaymentConfigModel, 'PaymentConfigStateModel', opts.sourceObject.paymentConfig));

autoMapper.createMap('ContactConfigStateModel', ContactConfigModel)
	.forAllMembers(autoMapper.copyOwnProperties)
	.forMember('shippingAddressConfig',
		opts => autoMapper.map('AddressConfigStateModel', AddressConfigModel, opts.sourceObject.shippingAddressConfig)
	);

autoMapper.createMap(ContactConfigModel, 'ContactConfigStateModel')
	.forAllMembers(autoMapper.copyDefinedProperties)
	.forMember('shippingAddressConfig',
		opts => autoMapper.map(AddressConfigModel, 'AddressConfigStateModel', opts.sourceObject.shippingAddressConfig)
	);

autoMapper.createMap('PaymentConfigStateModel', PaymentConfigModel)
	.forAllMembers(autoMapper.copyOwnProperties)
	.forMember('billingAddressConfig',
		opts => autoMapper.map('AddressConfigStateModel', AddressConfigModel, opts.sourceObject.billingAddressConfig)
	);

autoMapper.createMap(PaymentConfigModel, 'PaymentConfigStateModel')
	.forAllMembers(autoMapper.copyDefinedProperties)
	.forMember('billingAddressConfig',
		opts => autoMapper.map(AddressConfigModel, 'AddressConfigStateModel', opts.sourceObject.billingAddressConfig)
	);

autoMapper.createMap('ShoppingBagConfigStateModel', ShoppingBagConfigModel)
	.forAllMembers(autoMapper.copyOwnProperties);

autoMapper.createMap(ShoppingBagConfigModel, 'ShoppingBagConfigStateModel')
	.forAllMembers(autoMapper.copyDefinedProperties);

autoMapper.createMap('AddressConfigStateModel', AddressConfigModel)
	.forAllMembers(autoMapper.copyOwnProperties);

autoMapper.createMap(AddressConfigModel, 'AddressConfigStateModel')
	.forAllMembers(autoMapper.copyDefinedProperties);

autoMapper.createMap('StatesConfigStateModel', StatesConfigModel)
	.forAllMembers(autoMapper.copyOwnProperties);

autoMapper.createMap(StatesConfigModel, 'StatesConfigStateModel')
	.forAllMembers(autoMapper.copyDefinedProperties);
