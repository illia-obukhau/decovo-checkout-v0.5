const Module = require('common/helpers/module');

const commonModule = require('checkout/common/module');
const domainModule = require('checkout/domain/module');

require('checkout/state/mappingProfiles/orderMappingProfile');
require('checkout/state/mappingProfiles/contactInfoMappingProfile');
require('checkout/state/mappingProfiles/addressMappingProfile');
require('checkout/state/mappingProfiles/discountMappingProfile');
require('checkout/state/mappingProfiles/shoppingBagMappingProfile');
require('checkout/state/mappingProfiles/shoppingBagItemMappingProfile');
require('checkout/state/mappingProfiles/productMappingProfile');
require('checkout/state/mappingProfiles/paymentInfoMappingProfile');
require('checkout/state/mappingProfiles/cardInfoMappingProfile');
require('checkout/state/mappingProfiles/vendorConfigMappingProfile');

const stateModule = new Module(commonModule, domainModule);

module.exports = stateModule;
