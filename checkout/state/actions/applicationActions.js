const orderActions = require('./orderActions');
const vendorConfigActions = require('./vendorConfigActions');
const shoppingBagActions = require('./shoppingBagActions');
const discountActions = require('./discountActions');
const notFoundError = require('common/customErrors/notFoundError');

const onAppLoaded = function (orderId) {
	return (dispatch) => {
		return dispatch(vendorConfigActions.getVendorConfig(orderId));
	};
};

const onCheckoutLoaded = function (orderId) {
	return (dispatch) => {
		return Promise.all([
			dispatch(orderActions.getOrderById(orderId)),
			dispatch(shoppingBagActions.getShoppingBag(orderId))
		])
			.then((results) => {
				if (!results[0].value || !results[1].value) {
					throw notFoundError('No such order!');
				}
			});
	};
};

const onOrderLoaded = function (orderId) {
	return (dispatch) => {
		return dispatch(orderActions.getOrderById(orderId))
			.then((result) => {
				if (!result.value) {
					throw notFoundError('No such order!');
				}
			});
	};
};

const onOrderSubmit = function (order) {
	return (dispatch) => {
		return dispatch(orderActions.addOrder(order));
	};
};

const onShoppingBagChange = function (newShoppingBag) {
	return (dispatch) => {
		return dispatch(shoppingBagActions.updateShoppingBag(newShoppingBag))
			.then((result) => {
				return result.value;
			});
	};
};

const onPromoCodeApply = function (promoCode) {
	return (dispatch) => {
		return dispatch(discountActions.getByPromoCode(promoCode))
			.then((result) => {
				return result.value;
			});
	};
};

module.exports = {
	onAppLoaded,
	onPromoCodeApply,
	onCheckoutLoaded,
	onOrderLoaded,
	onOrderSubmit,
	onShoppingBagChange
};
