const actionTypes = require('checkout/state/constants/actionTypes');
const vendorConfigRepository = require('checkout/domain/repositories/vendorConfigRepository');
const defaultVendorConfig = require('checkout/domain/constants/defaultVendorConfig');
const autoMapper = require('checkout/common/helpers/autoMapper');
const VendorConfigModel = require('checkout/domain/models/vendorConfigModel');

const getVendorConfig = function (orderId) {
	return {
		type: actionTypes.GET_VENDOR_CONFIG,
		payload: vendorConfigRepository.getByOrderId(orderId)
			.then((vendorConfig) => {
				return vendorConfig || defaultVendorConfig;
			}),
		meta: {
			mapToState: (shoppingBag) => {
				return autoMapper.map(VendorConfigModel, 'VendorConfigStateModel', shoppingBag);
			}
		}
	};
};

module.exports = {
	getVendorConfig
};
