const actionTypes = require('checkout/state/constants/actionTypes');
const discountRepository = require('checkout/domain/repositories/discountRepository');

const getByPromoCode = function (promoCode) {
	return {
		type: actionTypes.GET_DISCOUNT,
		payload: discountRepository.getByPromoCode(promoCode)
	};
};

module.exports = {
	getByPromoCode
};
