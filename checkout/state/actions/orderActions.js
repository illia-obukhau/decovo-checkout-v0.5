const actionTypes = require('checkout/state/constants/actionTypes');
const autoMapper = require('checkout/common/helpers/autoMapper');
const OrderModel = require('checkout/domain/models/orderModel');
const orderRepository = require('checkout/domain/repositories/orderRepository');
const notifications = require('checkout/domain/constants/notifications');

const addOrder = function (order) {
	return {
		type: actionTypes.ADD_ORDER,
		payload: orderRepository.add(order),
		meta: {
			notification: notifications.order.add,
			mapToState: (addedOrder) => {
				return autoMapper.map(OrderModel, 'OrderStateModel', addedOrder);
			}
		}
	};
};

const getOrderById = function (orderId) {
	return {
		type: actionTypes.GET_ORDER,
		payload: orderRepository.getById(orderId),
		meta: {
			notification: notifications.order.get,
			mapToState: (orderToMap) => {
				return autoMapper.map(OrderModel, 'OrderStateModel', orderToMap);
			}
		}
	};
};

module.exports = {
	addOrder,
	getOrderById
};
