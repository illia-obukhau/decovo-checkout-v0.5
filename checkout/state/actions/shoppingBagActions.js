const actionTypes = require('checkout/state/constants/actionTypes');
const shoppingBagRepository = require('checkout/domain/repositories/shoppingBagRepository');
const autoMapper = require('checkout/common/helpers/autoMapper');
const ShoppingBagModel = require('checkout/domain/models/shoppingBagModel');

const updateShoppingBag = function (newShoppingBag) {
	return {
		type: actionTypes.UPDATE_SHOPPING_BAG,
		payload: shoppingBagRepository.update(newShoppingBag),
		meta: {
			mapToState: (shoppingBag) => {
				return autoMapper.map(ShoppingBagModel, 'ShoppingBagStateModel', shoppingBag);
			}
		}
	};
};

const getShoppingBag = function (shoppingBagId) {
	return {
		type: actionTypes.GET_SHOPPING_BAG,
		payload: shoppingBagRepository.get(shoppingBagId),
		meta: {
			mapToState: (shoppingBag) => {
				return autoMapper.map(ShoppingBagModel, 'ShoppingBagStateModel', shoppingBag);
			}
		}
	};
};

module.exports = {
	updateShoppingBag,
	getShoppingBag
};
