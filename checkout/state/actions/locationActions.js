const actionTypes = require('checkout/state/constants/actionTypes');
const locationRepository = require('checkout/domain/repositories/locationRepository');

const getCoordinatesByAddress = function (addressLine) {
	return {
		type: actionTypes.GET_COORDINATES,
		payload: locationRepository.getCoordinatesByAddress(addressLine)
	};
};

module.exports = {
	getCoordinatesByAddress
};
