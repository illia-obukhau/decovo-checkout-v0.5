const mapPayloadToStateModelMiddleware = store => next => (action) => {
	if (!action.error && action.payload && action.meta && action.meta.mapToState) {
		const mapToState = action.meta.mapToState;

		action.payload = mapToState(action.payload);
	}

	return next(action);
};

module.exports = mapPayloadToStateModelMiddleware;
