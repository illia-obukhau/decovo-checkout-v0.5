const actionTypes = require('checkout/state/constants/actionTypes');
const immutableMap = require('immutable').Map;
const { handleActions } = require('redux-actions');
const baseReducer = require('common/helpers/baseReducer');

const initialState = immutableMap();
const handlers = {
	[actionTypes.GET_SHOPPING_BAG_PENDING]: baseReducer.pending,
	[actionTypes.GET_SHOPPING_BAG_SUCCESS]: baseReducer.success,
	[actionTypes.GET_SHOPPING_BAG_ERROR]: baseReducer.error,
	[actionTypes.UPDATE_SHOPPING_BAG_PENDING]: baseReducer.pending,
	[actionTypes.UPDATE_SHOPPING_BAG_SUCCESS]: baseReducer.success,
	[actionTypes.UPDATE_SHOPPING_BAG_ERROR]: baseReducer.error
};

module.exports = handleActions(handlers, initialState);
