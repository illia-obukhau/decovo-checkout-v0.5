const actionTypes = require('checkout/state/constants/actionTypes');
const immutableMap = require('immutable').Map;
const { handleActions } = require('redux-actions');
const baseReducer = require('common/helpers/baseReducer');

const initialState = immutableMap();
const handlers = {
	[actionTypes.ADD_ORDER_PENDING]: baseReducer.pending,
	[actionTypes.ADD_ORDER_SUCCESS]: baseReducer.success,
	[actionTypes.ADD_ORDER_ERROR]: baseReducer.error,
	[actionTypes.GET_ORDER_PENDING]: baseReducer.pending,
	[actionTypes.GET_ORDER_SUCCESS]: baseReducer.success,
	[actionTypes.GET_ORDER_ERROR]: baseReducer.error
};

module.exports = handleActions(handlers, initialState);
