const actionTypes = require('checkout/state/constants/actionTypes');
const immutableMap = require('immutable').Map;
const { handleActions } = require('redux-actions');
const baseReducer = require('common/helpers/baseReducer');

const initialState = immutableMap();
const handlers = {
	[actionTypes.GET_VENDOR_CONFIG_PENDING]: baseReducer.pending,
	[actionTypes.GET_VENDOR_CONFIG_SUCCESS]: baseReducer.success,
	[actionTypes.GET_VENDOR_CONFIG_ERROR]: baseReducer.error
};

module.exports = handleActions(handlers, initialState);
