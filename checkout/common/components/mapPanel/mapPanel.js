const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');
const isEqual = require('lodash/isEqual');

const { Map, TileLayer, Marker } = require('react-leaflet');

require('./mapPanel.less');
require('leaflet/dist/leaflet.css');
require('leaflet/dist/images/marker-shadow.png');

class MapPanel extends Component {
	componentWillReceiveProps(nextProps) {
		const oldMarkerPosition = this.props.markerPosition;
		const nextMarkerPosition = nextProps.markerPosition;

		if (!isEqual(oldMarkerPosition, nextMarkerPosition)) {
			this.map.flyTo(nextMarkerPosition);
			this.marker.setLatLng(nextMarkerPosition);
		}
	}

	render() {
		const mapCenter = this.props.markerPosition;
		const zoomLevel = this.props.zoomLevel;

		return (
			<div className="map-panel">
				<Map
					center={mapCenter}
					zoom={zoomLevel}
					refs={(element) => {
						this.map = element;
					}}>
					<TileLayer
						url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
						attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
					/>
					<Marker
						position={mapCenter}
						refs={(element) => {
							this.marker = element;
						}} />
				</Map>
			</div>
		);
	}
}

MapPanel.propTypes = {
	markerPosition: PropTypes.arrayOf(PropTypes.number),
	zoomLevel: PropTypes.number.isRequired
};

module.exports = MapPanel;
