const React = require('react');
const { PropTypes } = require('prop-types');

const { Component } = React;
const classnames = require('classnames');

const MaskedInput = require('common/components/maskedInput');

require('./maskedTextField.less');

class MaskedTextField extends Component {
	renderValidationMessage(isInside) {
		const { meta } = this.props;
		const containerClassNames = classnames('masked-text-field__validation-message-container', {
			'masked-text-field__validation-message-container--outside': !isInside,
			'masked-text-field__validation-message-container--inside': isInside
		});

		if ((meta.touched || meta.submitFailed) && meta.error) {
			return (
				<div className={containerClassNames}>
					<div className="masked-text-field__validation-message">{meta.error}</div>
				</div>
			);
		}

		return null;
	}

	render() {
		const { className, placeholder, onClick, mask, input, meta } = this.props;
		const fieldClassNames = classnames(className, 'masked-text-field');

		return (
			<div className={fieldClassNames}>
				<div className="masked-text-field__inner-container">
					<div className="masked-text-field__input-container">
						<MaskedInput
							placeholder={placeholder}
							onClick={onClick}
							isInvalid={meta.invalid && (meta.touched || meta.submitFailed)}
							mask={mask}
							{...input}
						/>
						{this.renderValidationMessage(true)}
					</div>
				</div>
			</div>
		);
	}
}

MaskedTextField.propTypes = {
	input: PropTypes.object,
	meta: PropTypes.object,
	className: PropTypes.string,
	placeholder: PropTypes.string,
	mask: PropTypes.string,
	onClick: PropTypes.func
};

module.exports = MaskedTextField;
