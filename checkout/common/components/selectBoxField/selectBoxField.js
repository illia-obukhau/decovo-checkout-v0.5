const React = require('react');
const { PropTypes } = require('prop-types');

const { Component } = React;
const classnames = require('classnames');

const SelectBoxOptionModel = require('common/components/selectBox/selectBoxOptionModel');
const SelectBox = require('common/components/selectBox');

require('./selectBoxField.less');

class SelectBoxField extends Component {
	constructor(props, context, updater) {
		super(props, context, updater);

		this.onChange = this.onChange.bind(this);
	}

	onChange(newValue) {
		this.props.input.onChange(newValue);
	}

	renderValidationMessage() {
		const { meta } = this.props;
		if ((meta.touched || meta.submitFailed) && meta.error) {
			return (
				<div className="select-box-field__validation-message-container select-box-field__validation-message-container--inside">
					<div className="select-box-field__validation-message">{meta.error}</div>
				</div>
			);
		}

		return null;
	}

	render() {
		const { className, options, placeholder, defaultOption, input, meta, disabled } = this.props;
		const fieldClassNames = classnames(className, 'select-box-field');
		const errorContainer = this.renderValidationMessage();

		return (
			<div className={fieldClassNames}>
				<div className="select-box-field__inner-container">
					<SelectBox
						options={options}
						placeholder={placeholder}
						defaultOption={defaultOption}
						disabled={disabled}
						isInvalid={(meta.touched || meta.submitFailed) && meta.invalid}
						{...input}
						onChange={this.onChange}
					/>
					{errorContainer}
				</div>
			</div>
		);
	}
}

SelectBoxField.propTypes = {
	placeholder: PropTypes.string,
	input: PropTypes.object,
	meta: PropTypes.object,
	options: PropTypes.arrayOf(PropTypes.instanceOf(SelectBoxOptionModel)).isRequired,
	defaultOption: PropTypes.instanceOf(SelectBoxOptionModel),
	className: PropTypes.string,
	disabled: PropTypes.bool
};

SelectBoxField.defaultProps = {
	defaultOption: new SelectBoxOptionModel('', 'Select')
};

module.exports = SelectBoxField;
