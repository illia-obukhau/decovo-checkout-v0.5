const React = require('react');
const { PropTypes } = require('prop-types');

const { Component } = React;
const classnames = require('classnames');

const Input = require('common/components/input');

require('./textField.less');

class TextField extends Component {
	renderValidationMessage(isInside) {
		const { meta } = this.props;

		const containerClassNames = classnames('text-field__validation-message-container', {
			'text-field__validation-message-container--outside': !isInside,
			'text-field__validation-message-container--inside': isInside
		});

		if ((meta.touched || meta.submitFailed) && meta.error) {
			return (
				<div className={containerClassNames}>
					<div className="text-field__validation-message">{meta.error}</div>
				</div>
			);
		}

		return null;
	}

	render() {
		const { className, placeholder, onClick, input, meta } = this.props;
		const fieldClassNames = classnames(className, 'text-field');

		return (
			<div className={fieldClassNames}>
				<div className="text-field__inner-container">
					<div className="text-field__input-container">
						<Input
							placeholder={placeholder}
							onClick={onClick}
							isInvalid={(meta.touched || meta.submitFailed) && meta.invalid}
							{...input}
						/>
						{this.renderValidationMessage(true)}
					</div>
				</div>
			</div>
		);
	}
}

TextField.propTypes = {
	input: PropTypes.object,
	meta: PropTypes.object,
	className: PropTypes.string,
	placeholder: PropTypes.string,
	onClick: PropTypes.func
};

module.exports = TextField;
