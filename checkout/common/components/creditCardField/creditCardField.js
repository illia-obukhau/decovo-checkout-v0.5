const React = require('react');
const { PropTypes } = require('prop-types');

const { Component } = React;
const classnames = require('classnames');

const CreditCardInput = require('common/components/creditCardInput');
require('./creditCardField.less');

class CreditCardField extends Component {
	renderValidationMessage(isInside) {
		const { meta } = this.props;

		const containerClassNames = classnames('credit-card-field__validation-message-container', {
			'credit-card-field__validation-message-container--outside': !isInside,
			'credit-card-filed__validation-message-container--inside': isInside
		});

		if ((meta.touched || meta.submitFailed) && meta.error) {
			return (
				<div className={containerClassNames}>
					<div className="credit-card-field__validation-message">{meta.error}</div>
				</div>
			);
		}

		return null;
	}

	render() {
		const { className, placeholder, input, meta } = this.props;
		const fieldClassNames = classnames(className, 'credit-card-field');

		return (
			<div className={fieldClassNames}>
				<div className="credit-card-field__inner-container">
					<div className="credit-card-field__input-container">
						<CreditCardInput placeholder={placeholder} {...input} isInvalid={meta.invalid && (meta.touched || meta.submitFailed)} />
						{this.renderValidationMessage(true)}
					</div>
				</div>
			</div>
		);
	}
}

CreditCardField.propTypes = {
	input: PropTypes.object.isRequired,
	meta: PropTypes.object.isRequired,
	className: PropTypes.string,
	placeholder: PropTypes.string
};

CreditCardField.defaultProps = {
	className: '',
	placeholder: ''
};

module.exports = CreditCardField;
