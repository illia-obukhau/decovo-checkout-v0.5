const window = require('global/window');

class Printer {
	print(data, title) {
		if (!data) {
			return false;
		}
		const printingTitle = title || 'Print';
		const printingData = data.innerHTML || data;
		const modalWindow = window.open('', printingTitle, 'height=900,width=900');

		modalWindow.document.write(`
            <html>
                <head>
                    <title>${printingTitle}</title>
                    <link rel="stylesheet" href="/checkout/index.css" type="text/css"/>
                </head>
                <body>${printingData}</body>
            </html>
        `);

		modalWindow.document.close();
		modalWindow.focus();

		modalWindow.onload = function () {
			modalWindow.print();

			// This is used to fix an issue with closing printing window.
			setTimeout(() => {
				modalWindow.focus();
				modalWindow.close();
			}, 350);
		};

		return true;
	}
}

module.exports = Printer;
