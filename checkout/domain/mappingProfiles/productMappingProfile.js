const autoMapper = require('checkout/common/helpers/autoMapper');
const ProductModel = require('checkout/domain/models/productModel');

autoMapper.createMap('ProductDataModel', ProductModel)
	.forAllMembers(autoMapper.copyOwnProperties);

autoMapper.createMap(ProductModel, 'ProductDataModel')
	.forAllMembers(autoMapper.copyDefinedProperties);

