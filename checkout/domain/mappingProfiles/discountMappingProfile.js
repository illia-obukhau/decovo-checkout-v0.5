const autoMapper = require('checkout/common/helpers/autoMapper');
const DiscountModel = require('checkout/domain/models/discountModel');

autoMapper.createMap('DiscountDataModel', DiscountModel)
	.forAllMembers(autoMapper.copyOwnProperties);

autoMapper.createMap(DiscountModel, 'DiscountDataModel')
	.forAllMembers(autoMapper.copyDefinedProperties);

