const autoMapper = require('checkout/common/helpers/autoMapper');
const AddressModel = require('checkout/domain/models/addressModel');
const CardInfoModel = require('checkout/domain/models/cardInfoModel');
const PaymentInfoModel = require('checkout/domain/models/paymentInfoModel');

autoMapper.createMap('PaymentInfoDataModel', PaymentInfoModel)
	.forAllMembers(autoMapper.copyOwnProperties)
	.forMember('billingAddress', opts => autoMapper.map('AddressDataModel', AddressModel, opts.sourceObject.billingAddress))
	.forMember('cardInfo', opts => autoMapper.map('CardInfoDataModel', CardInfoModel, opts.sourceObject.cardInfo));

autoMapper.createMap(PaymentInfoModel, 'PaymentInfoDataModel')
	.forAllMembers(autoMapper.copyDefinedProperties)
	.forMember('billingAddress', opts => autoMapper.map(AddressModel, 'AddressDataModel', opts.sourceObject.billingAddress))
	.forMember('cardInfo', opts => autoMapper.map(CardInfoModel, 'CardInfoDataModel', opts.sourceObject.cardInfo));
