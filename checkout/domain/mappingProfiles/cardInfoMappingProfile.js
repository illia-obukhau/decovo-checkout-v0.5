const autoMapper = require('checkout/common/helpers/autoMapper');
const CardInfoModel = require('checkout/domain/models/cardInfoModel');

autoMapper.createMap('CardInfoDataModel', CardInfoModel)
	.forAllMembers(autoMapper.copyOwnProperties);

autoMapper.createMap(CardInfoModel, 'CardInfoDataModel')
	.forAllMembers(autoMapper.copyDefinedProperties);
