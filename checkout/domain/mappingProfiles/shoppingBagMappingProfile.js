const autoMapper = require('checkout/common/helpers/autoMapper');
const ShoppingBagModel = require('checkout/domain/models/shoppingBagModel');
const ShoppingBagItemModel = require('checkout/domain/models/shoppingBagItemModel');

autoMapper.createMap('ShoppingBagDataModel', ShoppingBagModel)
	.forAllMembers(autoMapper.copyOwnProperties)
	.forMember('items', opts => autoMapper.map('ShoppingBagItemStateModel', ShoppingBagItemModel, opts.sourceObject.items));

autoMapper.createMap(ShoppingBagModel, 'ShoppingBagDataModel')
	.forAllMembers(autoMapper.copyDefinedProperties)
	.forMember('items', opts => autoMapper.map(ShoppingBagItemModel, 'ShoppingBagItemDataModel', opts.sourceObject.items));

