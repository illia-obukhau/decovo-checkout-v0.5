const autoMapper = require('checkout/common/helpers/autoMapper');
const AddressModel = require('checkout/domain/models/addressModel');

autoMapper.createMap('AddressDataModel', AddressModel)
	.forAllMembers(autoMapper.copyOwnProperties);

autoMapper.createMap(AddressModel, 'AddressDataModel')
	.forAllMembers(autoMapper.copyDefinedProperties);

