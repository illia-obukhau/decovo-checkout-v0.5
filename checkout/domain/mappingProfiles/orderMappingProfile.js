const autoMapper = require('checkout/common/helpers/autoMapper');
const OrderModel = require('checkout/domain/models/orderModel');
const ShoppingBagModel = require('checkout/domain/models/shoppingBagModel');
const ContactInfoModel = require('checkout/domain/models/contactInfoModel');
const PaymentInfoModel = require('checkout/domain/models/paymentInfoModel');
const DiscountModel = require('checkout/domain/models/discountModel');

autoMapper.createMap('OrderDataModel', OrderModel)
	.forAllMembers(autoMapper.copyOwnProperties)
	.forMember('contactInfo', opts => autoMapper.map('ContactInfoDataModel', ContactInfoModel, opts.sourceObject.contactInfo))
	.forMember('shoppingBag', opts => autoMapper.map('ShoppingBagDataModel', ShoppingBagModel, opts.sourceObject.shoppingBag))
	.forMember('paymentInfo', opts => autoMapper.map('PaymentInfoDataModel', PaymentInfoModel, opts.sourceObject.paymentInfo))
	.forMember('discounts', opts => autoMapper.mapArray('DiscountDataModel', DiscountModel, opts.sourceObject.discounts));

autoMapper.createMap(OrderModel, 'OrderDataModel')
	.forAllMembers(autoMapper.copyDefinedProperties)
	.forMember('contactInfo', opts => autoMapper.map(ContactInfoModel, 'ContactInfoDataModel', opts.sourceObject.contactInfo))
	.forMember('shoppingBag', opts => autoMapper.map(ShoppingBagModel, 'ShoppingBagDataModel', opts.sourceObject.shoppingBag))
	.forMember('paymentInfo', opts => autoMapper.map(PaymentInfoModel, 'PaymentInfoDataModel', opts.sourceObject.paymentInfo))
	.forMember('discounts', opts => autoMapper.mapArray(DiscountModel, 'DiscountDataModel', opts.sourceObject.discounts));

