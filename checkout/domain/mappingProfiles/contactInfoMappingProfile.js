const autoMapper = require('checkout/common/helpers/autoMapper');
const ContactInfoModel = require('checkout/domain/models/contactInfoModel');
const AddressModel = require('checkout/domain/models/addressModel');

autoMapper.createMap('ContactInfoDataModel', ContactInfoModel)
	.forAllMembers(autoMapper.copyOwnProperties)
	.forMember('shippingAddress', opts => autoMapper.map('AddressDataModel', AddressModel, opts.sourceObject.shippingAddress));

autoMapper.createMap(ContactInfoModel, 'ContactInfoDataModel')
	.forAllMembers(autoMapper.copyDefinedProperties)
	.forMember('shippingAddress', opts => autoMapper.map(AddressModel, 'AddressDataModel', opts.sourceObject.shippingAddress));

