const autoMapper = require('checkout/common/helpers/autoMapper');
const VendorConfigModel = require('checkout/domain/models/vendorConfigModel');
const ContactConfigModel = require('checkout/domain/models/contactConfigModel');
const ShoppingBagConfigModel = require('checkout/domain/models/shoppingBagConfigModel');
const PaymentConfigModel = require('checkout/domain/models/paymentConfigModel');
const AddressConfigModel = require('checkout/domain/models/addressConfigModel');
const StatesConfigModel = require('checkout/domain/models/statesConfigModel');

autoMapper.createMap('VendorConfigDataModel', VendorConfigModel)
	.forAllMembers(autoMapper.copyOwnProperties)
	.forMember('contactConfig', opts => autoMapper.map('ContactConfigDataModel', ContactConfigModel, opts.sourceObject.contactConfig))
	.forMember('shoppingBagConfig',
		opts => autoMapper.map('ShoppingBagConfigDataModel', ShoppingBagConfigModel, opts.sourceObject.shoppingBagConfig)
	)
	.forMember('paymentConfig', opts => autoMapper.map('PaymentConfigDataModel', PaymentConfigModel, opts.sourceObject.paymentConfig));

autoMapper.createMap('ContactConfigDataModel', ContactConfigModel)
	.forAllMembers(autoMapper.copyOwnProperties)
	.forMember('shippingAddressConfig',
		opts => autoMapper.map('AddressConfigDataModel', AddressConfigModel, opts.sourceObject.shippingAddressConfig)
	);

autoMapper.createMap('ShoppingBagConfigDataModel', ShoppingBagConfigModel)
	.forAllMembers(autoMapper.copyOwnProperties);

autoMapper.createMap('PaymentConfigDataModel', PaymentConfigModel)
	.forAllMembers(autoMapper.copyOwnProperties)
	.forMember('billingAddressConfig',
		opts => autoMapper.map('AddressConfigDataModel', AddressConfigModel, opts.sourceObject.billingAddressConfig)
	);

autoMapper.createMap('AddressConfigDataModel', AddressConfigModel)
	.forAllMembers(autoMapper.copyOwnProperties)
	.forMember('statesConfig',
		opts => autoMapper.map('StatesConfigDataModel', StatesConfigModel, opts.sourceObject.statesConfig)
	);

autoMapper.createMap('StatesConfigDataModel', StatesConfigModel)
	.forAllMembers(autoMapper.copyOwnProperties);
