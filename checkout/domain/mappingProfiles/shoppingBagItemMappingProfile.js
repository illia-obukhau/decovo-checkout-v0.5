const autoMapper = require('checkout/common/helpers/autoMapper');
const ShoppingBagItemModel = require('checkout/domain/models/shoppingBagItemModel');
const ProductModel = require('checkout/domain/models/productModel');

autoMapper.createMap('ShoppingBagItemDataModel', ShoppingBagItemModel)
	.forAllMembers(autoMapper.copyOwnProperties)
	.forMember('product', opts => autoMapper.map('ProductDataModel', ProductModel, opts.sourceObject.product));

autoMapper.createMap(ShoppingBagItemModel, 'ShoppingBagItemDataModel')
	.forAllMembers(autoMapper.copyDefinedProperties)
	.forMember('product', opts => autoMapper.map(ProductModel, 'ProductDataModel', opts.sourceObject.product));

