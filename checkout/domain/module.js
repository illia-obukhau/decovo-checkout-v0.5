const Module = require('common/helpers/module');

const commonModule = new Module();

require('checkout/domain/mappingProfiles/addressMappingProfile');
require('checkout/domain/mappingProfiles/contactInfoMappingProfile');
require('checkout/domain/mappingProfiles/paymentInfoMappingProfile');
require('checkout/domain/mappingProfiles/cardInfoMappingProfile');
require('checkout/domain/mappingProfiles/orderMappingProfile');
require('checkout/domain/mappingProfiles/productMappingProfile');
require('checkout/domain/mappingProfiles/shoppingBagItemMappingProfile');
require('checkout/domain/mappingProfiles/shoppingBagMappingProfile');
require('checkout/domain/mappingProfiles/productMappingProfile');
require('checkout/domain/mappingProfiles/vendorConfigMappingProfile');
require('checkout/domain/mappingProfiles/discountMappingProfile');

module.exports = commonModule;
