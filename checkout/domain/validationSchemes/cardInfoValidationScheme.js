
module.exports = {
	fields: {
		cardNumber: {
			isRequired: true
		},
		cardHolder: {
			isRequired: true,
			minLength: 3,
			maxLength: 40
		},
		expirationMonth: {
			isRequired: true
		},
		expirationYear: {
			isRequired: true
		},
		cvv: {
			isRequired: true,
			minLength: 3,
			maxLength: 4
		}
	}
};
