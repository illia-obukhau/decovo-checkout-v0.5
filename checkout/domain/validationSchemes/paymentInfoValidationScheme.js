const addressValidationScheme = require('./addressValidationScheme');
const cardInfoValidationScheme = require('./cardInfoValidationScheme');

const billingAddressValidationScheme = Object.assign({}, addressValidationScheme);

billingAddressValidationScheme.isOptional = true;

module.exports = {
	fieldSets: {
		cardInfo: cardInfoValidationScheme,
		billingAddress: billingAddressValidationScheme
	}
};
