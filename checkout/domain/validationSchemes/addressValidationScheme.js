module.exports = {
	fields: {
		firstName: {
			isRequired: true,
			minLength: 2,
			maxLength: 40
		},
		lastName: {
			isRequired: true,
			minLength: 2,
			maxLength: 40
		},
		companyName: {
			isRequired: false,
			minLength: 2,
			maxLength: 40
		},
		streetAddress: {
			isRequired: true,
			minLength: 2,
			maxLength: 40
		},
		addressDetails: {
			isRequired: true,
			minLength: 2,
			maxLength: 40
		},
		city: {
			isRequired: true,
			minLength: 2,
			maxLength: 40
		},
		state: {
			isRequired: true,
			minLength: 2,
			maxLength: 40
		},
		zipCode: {
			isRequired: true,
			minLength: 2,
			maxLength: 40
		},
		country: {
			isRequired: true,
			minLength: 2,
			maxLength: 40
		}
	}
};
