const contactInfoValidationScheme = require('./contactInfoValidationScheme');
const paymentInfoValidationScheme = require('./paymentInfoValidationScheme');

module.exports = {
	fieldSets: {
		contactInfo: contactInfoValidationScheme,
		paymentInfo: paymentInfoValidationScheme
	}
};
