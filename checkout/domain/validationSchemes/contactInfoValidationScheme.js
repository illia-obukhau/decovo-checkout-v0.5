const addressValidationScheme = require('./addressValidationScheme');

module.exports = {
	fields: {
		email: {
			isRequired: true,
			isEmail: true
		},
		phone: {
			isRequired: false,
			minLength: 10
		}
	},
	fieldSets: {
		shippingAddress: addressValidationScheme
	}
};
