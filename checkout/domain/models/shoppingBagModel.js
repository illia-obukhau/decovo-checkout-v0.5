class ShoppingBagModel {
	constructor(id, items) {
		this.id = id;
		this.items = items;
	}
}

module.exports = ShoppingBagModel;
