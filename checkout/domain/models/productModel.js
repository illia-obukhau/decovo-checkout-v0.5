class ProductModel {
	constructor(id, title, imageUrl, price, shippingPrice, taxPercent) {
		this.id = id;
		this.title = title;
		this.imageUrl = imageUrl;
		this.price = price;
		this.shippingPrice = shippingPrice;
		this.taxPercent = taxPercent;
	}
}

module.exports = ProductModel;
