class ShoppingBagEditorModel {
	constructor(shoppingBag, removedItemIds, currentItemOperationsDictionary) {
		this.shoppingBag = shoppingBag;
		this.removedItemIds = removedItemIds;
		this.currentItemOperationsDictionary = currentItemOperationsDictionary;
	}
}

module.exports = ShoppingBagEditorModel;
