const currencyHelper = require('common/helpers/currencyHelper');

class OrderModel {
	constructor(id, contactInfo, shoppingBag, discounts, paymentInfo, orderStatus) {
		this.id = id;
		this.contactInfo = contactInfo;
		this.shoppingBag = shoppingBag;
		this.discounts = discounts;
		this.paymentInfo = paymentInfo;
		this.orderStatus = orderStatus;
	}

	getSubtotalPrice() {
		let result = 0;
		if (this.shoppingBag && this.shoppingBag.items.length) {
			this.shoppingBag.items.forEach((item) => {
				result += item.product.price * item.amount;
			});
		}

		return result;
	}

	getShippingPrice() {
		let result = 0;
		if (this.shoppingBag && this.shoppingBag.items.length) {
			this.shoppingBag.items.forEach((item) => {
				result += item.product.shippingPrice * item.amount;
			});
		}

		return result;
	}

	getTaxPrice() {
		let result = 0;
		if (this.shoppingBag && this.shoppingBag.items.length) {
			this.shoppingBag.items.forEach((item) => {
				result += currencyHelper.calculateTaxPrice(item.product.price * item.amount, item.product.taxPercent);
			});
		}

		return currencyHelper.round(result, 0);
	}

	getDiscountPrice(discountBase, discount) {
		const discountPrice = currencyHelper.calculateTaxPrice(discountBase, discount.amount);

		return currencyHelper.round(discountPrice, 0);
	}

	getRawTotalPrice() {
		const subTotalPrice = this.getSubtotalPrice();
		const shippingPrice = this.getShippingPrice();
		const taxPrice = this.getTaxPrice();

		return subTotalPrice + shippingPrice + taxPrice;
	}

	getTotalPrice() {
		const rawTotalPrice = this.getRawTotalPrice();

		let discountPrice = 0;
		if (this.discounts && this.discounts.length) {
			discountPrice += this.getDiscountPrice(rawTotalPrice, this.discounts[0]);
		}

		return rawTotalPrice - discountPrice;
	}
}

module.exports = OrderModel;
