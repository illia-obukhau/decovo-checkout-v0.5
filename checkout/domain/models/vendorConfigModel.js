class VendorConfigModel {
	constructor(vendorId, contactConfig, shoppingBagConfig, paymentConfig) {
		this.vendorId = vendorId;
		this.contactConfig = contactConfig;
		this.shoppingBagConfig = shoppingBagConfig;
		this.paymentConfig = paymentConfig;
	}
}

module.exports = VendorConfigModel;
