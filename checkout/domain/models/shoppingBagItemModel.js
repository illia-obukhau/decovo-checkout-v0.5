class ShoppingBagItemModel {
	constructor(id, product, amount) {
		this.id = id;
		this.product = product;
		this.amount = amount;
	}
}

module.exports = ShoppingBagItemModel;
