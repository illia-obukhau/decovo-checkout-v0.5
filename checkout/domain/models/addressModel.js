const isEqual = require('lodash/isEqual');

class AddressModel {
	constructor(firstName, lastName, companyName, streetAddress, addressDetails, city, state, zipCode, country) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.companyName = companyName;
		this.streetAddress = streetAddress;
		this.addressDetails = addressDetails;
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
		this.country = country;
	}

	getAddressLine() {
		return [
			this.streetAddress,
			this.addressDetails,
			this.city,
			this.state,
			this.country,
			this.zipCode
		].join(', ');
	}

	isEqual(address) {
		return isEqual(this, address);
	}
}

module.exports = AddressModel;
