class PaymentConfigModel {
	constructor(allowedPaymentTypes, billingAddressConfig) {
		this.allowedPaymentTypes = allowedPaymentTypes;
		this.billingAddressConfig = billingAddressConfig;
	}
}

module.exports = PaymentConfigModel;
