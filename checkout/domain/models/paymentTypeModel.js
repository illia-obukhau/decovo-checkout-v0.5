class PaymentTypeModel {
	constructor(id, name, isInternal, brandImgUrl) {
		this.id = id;
		this.name = name;
		this.isInternal = isInternal;
		this.brandImgUrl = brandImgUrl;
	}
}

module.exports = PaymentTypeModel;
