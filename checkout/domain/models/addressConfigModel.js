class AddressConfigModel {
	constructor(note, countriesAllowed, statesConfig) {
		this.note = note;
		this.countriesAllowed = countriesAllowed;
		this.statesConfig = statesConfig;
	}
}

module.exports = AddressConfigModel;
