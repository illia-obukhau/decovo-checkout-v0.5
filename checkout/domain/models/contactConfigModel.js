class AddressConfigModel {
	constructor(shippingAddressConfig, validationScheme) {
		this.shippingAddressConfig = shippingAddressConfig;
		this.validationScheme = validationScheme;
	}
}

module.exports = AddressConfigModel;
