class ContactInfoModel {
	constructor(email, shippingAddress, phone) {
		this.email = email;
		this.shippingAddress = shippingAddress;
		this.phone = phone;
	}
}

module.exports = ContactInfoModel;
