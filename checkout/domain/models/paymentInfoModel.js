class PaymentInfoModel {
	constructor(billingAddress, cardInfo) {
		this.billingAddress = billingAddress;
		this.cardInfo = cardInfo;
	}
}

module.exports = PaymentInfoModel;
