class StatesConfigModel {
	constructor(countryId, statesAllowed) {
		this.countryId = countryId;
		this.statesAllowed = statesAllowed;
	}
}

module.exports = StatesConfigModel;
