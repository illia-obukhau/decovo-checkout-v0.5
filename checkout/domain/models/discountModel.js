class DiscountModel {
	constructor(id, type, amount, meta) {
		this.id = id;
		this.type = type;
		this.amount = amount;
		this.meta = meta;
	}
}

module.exports = DiscountModel;
