const autoMapper = require('checkout/common/helpers/autoMapper');
const DiscountModel = require('checkout/domain/models/discountModel');
const fakePromoCodes = require('checkout/domain/constants/fakePromoCodes');

module.exports.getByPromoCode = function (promoCode) {
	return new Promise((resolve, reject) => {
		const targetDiscount = fakePromoCodes.find(item => item.meta && item.meta.promoCode === promoCode);
		const discountModel = autoMapper.map('DiscountDataModel', DiscountModel, targetDiscount);

		setTimeout(resolve, 500, discountModel);
	});
};
