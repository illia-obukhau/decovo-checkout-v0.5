const autoMapper = require('checkout/common/helpers/autoMapper');
const ShoppingBagModel = require('checkout/domain/models/shoppingBagModel');
const fakeShoppingBag = require('checkout/domain/constants/fakeShoppingBag');

module.exports.update = function (newShoppingBag) {
	return new Promise((resolve, reject) => {
		setTimeout(resolve, 500, newShoppingBag);
	});
};

module.exports.get = function (shoppingBagId) {
	return new Promise((resolve, reject) => {
		if (shoppingBagId === '1111') {
			resolve(autoMapper.map('ShoppingBagDataModel', ShoppingBagModel, fakeShoppingBag));
		}

		resolve(null);
	});
};

