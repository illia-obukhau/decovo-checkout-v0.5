const autoMapper = require('checkout/common/helpers/autoMapper');
const OrderModel = require('checkout/domain/models/orderModel');
const fakeOrder = require('checkout/domain/constants/fakeOrder');
const fakePaidOrder = require('checkout/domain/constants/fakePaidOrder');

module.exports.getById = function (orderId) {
	return new Promise((resolve, reject) => {
		let order = null;

		if (orderId === '1111') {
			order = autoMapper.map('OrderDataModel', OrderModel, fakeOrder);
		} else if (orderId === '2222') {
			order = autoMapper.map('OrderDataModel', OrderModel, fakePaidOrder);
		}

		setTimeout(resolve, 500, order);
	});
};

module.exports.add = function (order) {
	return new Promise((resolve, reject) => {
		const addedOrder = Object.assign(new OrderModel(), order);
		addedOrder.id = 1232;
		addedOrder.orderStatus = 'SUBMITTED';

		setTimeout(resolve, 1000, addedOrder);
	});
};

