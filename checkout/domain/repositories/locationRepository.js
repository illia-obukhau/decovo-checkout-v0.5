const ajax = require('common/helpers/ajax');
const templateHelper = require('common/helpers/templateHelper');

const serviceUrlTemplate = `https://search.mapzen.com/v1/search?api_key=${process.env.MAP_SERVICE_API_KEY}&text={addressLine}`;

module.exports.getCoordinatesByAddress = function (addressLine) {
	const addressUrlComponent = encodeURIComponent(addressLine);
	const url = templateHelper.format(serviceUrlTemplate, { addressLine: addressUrlComponent });

	return ajax.get(url)
		.then((response) => {
			const targetFeature = response.features.find(feature => !!feature.geometry);

			return targetFeature.geometry.coordinates;
		});
};

