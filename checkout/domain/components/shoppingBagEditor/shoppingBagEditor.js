const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');
const resources = require('checkout/common/constants/resources');

const currencyFormats = require('common/constants/currencyFormats');
const operationTypes = require('checkout/domain/components/shoppingBagEditor/operationTypes');

const ShoppingBagConfigModel = require('checkout/domain/models/shoppingBagConfigModel');
const ShoppingBagModel = require('checkout/domain/models/shoppingBagModel');
const ShoppingBagItem = require('checkout/domain/components/shoppingBagItem');
const OperationLoader = require('common/components/operationLoader');
require('./shoppingBagEditor.less');

class ShoppingBagEditor extends Component {
	constructor(props, context, updater) {
		super(props, context, updater);

		this.state = {
			editorItems: [],
			idToOperationDictionary: {}
		};

		this.onItemAmountChange = this.onItemAmountChange.bind(this);
		this.onItemRemoveClick = this.onItemRemoveClick.bind(this);
		this.onItemUndoClick = this.onItemUndoClick.bind(this);
		this.onItemsChange = this.onItemsChange.bind(this);
	}

	componentWillMount() {
		const { model } = this.props;

		if (model && model.items) {
			this.setState({
				editorItems: model.items,
				idToOperationDictionary: {}
			});
		}
	}

	componentWillReceiveProps(nextProps) {
		const { editorItems } = this.state;

		nextProps.model.items.forEach((existingItem) => {
			const relatedEditorItem = editorItems.find(editorItem => editorItem.id === existingItem.id);

			if (relatedEditorItem) {
				relatedEditorItem.amount = existingItem.amount;
			}
		});

		this.setState({ editorItems });
	}

	onItemsChange(newShoppingBag, processedItemId, operationType) {
		this.setProcessingNotification(processedItemId, operationType);

		this.props.onShoppingBagChange(newShoppingBag)
			.then(() => {
				this.setProcessingNotification(processedItemId, null);
			});
	}

	onItemAmountChange(targetItemId, newAmount) {
		const newShoppingBag = Object.assign(new ShoppingBagModel(), this.props.model);
		const targetItem = newShoppingBag.items.find(item => item.id === targetItemId);
		targetItem.amount = newAmount;

		this.onItemsChange(newShoppingBag, targetItemId, operationTypes.UPDATE);
	}

	onItemRemoveClick(targetItemId) {
		const newShoppingBag = Object.assign(new ShoppingBagModel(), this.props.model);
		newShoppingBag.items = newShoppingBag.items.filter(item => item.id !== targetItemId);

		this.onItemsChange(newShoppingBag, targetItemId, operationTypes.DELETE);
	}

	onItemUndoClick(targetItemId) {
		const { editorItems } = this.state;

		const newShoppingBag = Object.assign(new ShoppingBagModel(), this.props.model);
		const targetItem = editorItems.find(editorItem => editorItem.id === targetItemId);
		newShoppingBag.items.push(targetItem);

		this.onItemsChange(newShoppingBag, targetItemId, operationTypes.RESTORE);
	}

	setProcessingNotification(itemId, operationType) {
		const idToOperationDictionary = Object.assign(this.state.idToOperationDictionary);
		idToOperationDictionary[itemId] = operationType;

		this.setState({ idToOperationDictionary });
	}

	renderItems() {
		const { model, isReadOnly } = this.props;
		const { editorItems, idToOperationDictionary } = this.state;

		const shoppingBagItemContainers = editorItems.map((editorItem, index) => {
			const key = `shopping-bag-item-${index}`;
			const isRemoved = !model.items.find(existingItem => editorItem.id === existingItem.id);
			const processingOperation = idToOperationDictionary[editorItem.id];

			if (isRemoved && !processingOperation) {
				return (
					<div className="shopping-bag-editor__item-container shopping-bag-editor__item-container--removed" key={key}>
						{this.renderRemovedItem(editorItem)}
					</div>
				);
			}

			return (
				<div className="shopping-bag-editor__item-container" key={key}>
					<OperationLoader
						isVisible={!!processingOperation}
						message={resources[`shoppingBag.operations.${processingOperation}`]}
						messageClass="shopping-bag-editor__operation-message"
					>
						<ShoppingBagItem
							isReadOnly={isReadOnly}
							model={editorItem}
							priceFormat={currencyFormats.currencyFormat}
							maxAmountForSelect={this.props.config.maxAmountForSelect}
							onRemoveClick={this.onItemRemoveClick}
							onAmountChange={this.onItemAmountChange}
							isBlured={!!processingOperation}
						/>
					</OperationLoader>
				</div>
			);
		});

		return (
			<div className="shopping-bag-editor__items">
				{shoppingBagItemContainers}
			</div>
		);
	}

	renderRemovedItem(item) {
		return (
			<div className="shopping-bag-editor__notification-container">
				<div className="shopping-bag-editor__message-container">
					<span className="shopping-bag-editor__message">{item.product.title} is removed.</span>
				</div>
				<div className="shopping-bag-editor__notification-control-container">
					<span className="shopping-bag-editor__link" onClick={this.onItemUndoClick.bind(this, item.id)}>Undo</span>
				</div>
			</div>
		);
	}

	render() {
		const itemsContainer = this.props.model ? this.renderItems() : null;

		return (
			<div className="shopping-bag-editor">
				<div className="shopping-bag-editor__inner-container">
					<div className="shopping-bag-editor__title-container">
						<h2 className="shopping-bag-editor__title">Shopping Bag</h2>
					</div>
					{itemsContainer}
				</div>
			</div>
		);
	}
}

ShoppingBagEditor.propTypes = {
	isReadOnly: PropTypes.bool,
	onShoppingBagChange: PropTypes.func,
	config: PropTypes.instanceOf(ShoppingBagConfigModel),
	model: PropTypes.instanceOf(ShoppingBagModel)
};

module.exports = ShoppingBagEditor;

