const React = require('react');

const { PropTypes } = require('prop-types');

const { Component } = React;
const { Field } = require('redux-form');
const resources = require('checkout/common/constants/resources');
const AddressConfigModel = require('checkout/domain/models/addressConfigModel');

const TextField = require('checkout/common/components/textField');
const SelectBoxField = require('checkout/common/components/selectBoxField');
const SelectBoxOptionModel = require('checkout/common/components/selectBoxField/selectBoxOptionModel');

require('./addressFieldSet.less');

class AddressFieldSet extends Component {
	constructor(props, context, updater) {
		super(props, context, updater);

		this.state = {
			states: []
		};

		this.onCountryChange = this.onCountryChange.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		const statesConfig = nextProps.config.statesConfig.find(config => !!config);

		this.setState({
			states: statesConfig.statesAllowed
		});
	}

	onCountryChange(event, newCountryId) {
		const statesConfig = this.props.config.statesConfig.find(config => config.countryId === newCountryId);

		this.setState({
			states: statesConfig.statesAllowed
		});

		this.props.onCountryChange(newCountryId);
	}

	render() {
		const statesOptions = this.state.states.map((item, index) => {
			return new SelectBoxOptionModel(item.id, item.name);
		});

		const countriesOptions = this.props.config.countriesAllowed.map((item) => {
			return new SelectBoxOptionModel(item.id, item.name);
		});
		const countriesDisabled = countriesOptions.length === 1;
		const defaultContryOption = countriesOptions[0];
		const labelContainer = !this.props.title ? null : (
			<div className="address-fieldset__header-container">
				<div className="address-fieldset__description-container">
					<div className="address-fieldset__description">{this.props.title}</div>
				</div>
				<div className="address-fieldset__note-container">
					<div className="address-fieldset__note">{this.props.config.note}</div>
				</div>
			</div>
		);

		return (
			<fieldset className="address-fieldset">
				<div className="address-fieldset__inner-container">
					{labelContainer}

					<div className="columns-layout">
						<div
							className="address-fieldset__column columns-layout__column columns-layout__column--lg-50 columns-layout__column--md-50">
							<div className="address-fieldset__input-container">
								<Field
									className="address-fieldset__input"
									name="firstName"
									component={TextField}
									placeholder={resources['address.placeholders.firstName']}
								/>
							</div>
						</div>
						<div className="address-fieldset__column columns-layout__column columns-layout__column--lg-50 columns-layout__column--md-50">
							<div className="address-fieldset__input-container">
								<Field
									className="address-fieldset__input"
									name="lastName"
									component={TextField}
									placeholder={resources['address.placeholders.lastName']}
								/>
							</div>
						</div>
					</div>
					<div className="address-fieldset__input-container">
						<Field
							className="address-fieldset__input"
							name="companyName"
							component={TextField}
							placeholder={resources['address.placeholders.companyName']}
						/>
					</div>
					<div className="address-fieldset__input-container">
						<Field
							className="address-fieldset__input"
							name="streetAddress"
							component={TextField}
							placeholder={resources['address.placeholders.streetAddress']}
						/>
					</div>
					<div className="address-fieldset__input-container">
						<Field
							className="address-fieldset__input"
							name="addressDetails"
							component={TextField}
							placeholder={resources['address.placeholders.addressDetails']}
						/>
					</div>
					<div className="address-fieldset__input-container">
						<Field
							className="address-fieldset__input"
							name="city"
							component={TextField}
							placeholder={resources['address.placeholders.city']}
						/>
					</div>

					<div className="address-fieldset__input-container">
						<Field
							className="address-fieldset__input"
							name="country"
							component={SelectBoxField}
							options={countriesOptions}
							defaultOption={defaultContryOption}
							onChange={this.onCountryChange}
							disabled={countriesDisabled}
						/>
					</div>

					<div className="columns-layout">
						<div className="address-fieldset__column columns-layout__column columns-layout__column--lg-50 columns-layout__column--md-50">
							<div className="address-fieldset__input-container">
								<Field
									className="address-fieldset__input"
									name="state"
									component={SelectBoxField}
									options={statesOptions}
									defaultOption={new SelectBoxOptionModel('', 'State')}
								/>
							</div>
						</div>
						<div className="address-fieldset__column columns-layout__column columns-layout__column--lg-50 columns-layout__column--md-50">
							<div className="address-fieldset__input-container">
								<Field
									className="address-fieldset__input"
									name="zipCode"
									component={TextField}
									placeholder={resources['address.placeholders.zipCode']}
								/>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
		);
	}
}

AddressFieldSet.propTypes = {
	title: PropTypes.string,
	config: PropTypes.instanceOf(AddressConfigModel),
	onCountryChange: PropTypes.func.isRequired
};

module.exports = AddressFieldSet;

