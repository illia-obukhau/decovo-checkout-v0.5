const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');

const ShoppingBagItemModel = require('checkout/domain/models/shoppingBagItemModel');

const AmountInput = require('common/components/amountInput');
const Price = require('common/components/price');

require('./shoppingBagItem.less');
const defaultImage = require('checkout/static/images/img-001.png');

class ShoppingBagItem extends Component {
	constructor(props, context, updater) {
		super(props, context, updater);

		this.onRemoveClick = this.onRemoveClick.bind(this);
		this.onAmountChange = this.onAmountChange.bind(this);
	}

	onRemoveClick() {
		const { onRemoveClick, isReadOnly, model: { id } } = this.props;

		if (!isReadOnly) {
			onRemoveClick(id);
		}
	}

	onAmountChange(newAmount) {
		const { onAmountChange, isReadOnly, model: { id } } = this.props;

		if (!isReadOnly) {
			onAmountChange(id, newAmount);
		}
	}

	renderAmount() {
		const { isReadOnly, model: { amount } } = this.props;

		if (!isReadOnly) {
			return (
				<div className="shopping-bag-item__amount-container">
					<AmountInput
						className="shopping-bag-item__amount-control"
						name="amount"
						value={amount}
						maxAmountForSelect={this.props.maxAmountForSelect}
						onChange={this.onAmountChange}
					/>
				</div>
			);
		}

		return (
			<div className="shopping-bag-item__amount-container">
				<div className="shopping-bag-item__amount-text">Quantity: {amount}</div>
			</div>
		);
	}

	renderControls() {
		const { isReadOnly } = this.props;

		if (!isReadOnly) {
			return (
				<div className="shopping-bag-item__controls-container">
					<span className="shopping-bag-item__link" onClick={this.onRemoveClick}>Remove</span>
				</div>
			);
		}

		return null;
	}

	render() {
		const { priceFormat, model } = this.props;
		const product = model.product;
		const amountContainer = this.renderAmount();
		const controlsContainer = this.renderControls();

		return (
			<div className="shopping-bag-item">
				<div className="shopping-bag-item__inner-container">
					<div className="shopping-bag-item__image-container">
						<img className="shopping-bag-item__image" alt={product.title} src={product.imageUrl || defaultImage} />
					</div>

					<div className="shopping-bag-item__content">
						<div className="shopping-bag-item__title-container">
							<span className="shopping-bag-item__title">{product.title}</span>
						</div>

						<div className="shopping-bag-item__adaptive-container">
							{amountContainer}

							<div className="shopping-bag-item__price-container">
								<Price className="shopping-bag-item__price" value={product.price} format={priceFormat} />
							</div>

							{controlsContainer}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

ShoppingBagItem.propTypes = {
	isReadOnly: PropTypes.bool,
	model: PropTypes.instanceOf(ShoppingBagItemModel).isRequired,
	maxAmountForSelect: PropTypes.number,
	onAmountChange: PropTypes.func,
	onRemoveClick: PropTypes.func.isRequired,
	priceFormat: PropTypes.string
};

module.exports = ShoppingBagItem;

