const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');
const { reduxForm } = require('redux-form/immutable');
const { FormSection } = require('redux-form');
const resources = require('checkout/common/constants/resources');
const AddressModel = require('checkout/domain/models/addressModel');
const PaymentConfigModel = require('checkout/domain/models/paymentConfigModel');

const validateByScheme = require('checkout/features/validation/validateByScheme');
const paymentInfoValidationScheme = require('checkout/domain/validationSchemes/paymentInfoValidationScheme');
const CardInfoFieldSet = require('checkout/domain/components/cardInfoFieldSet');
const AddressFieldSet = require('checkout/domain/components/addressFieldSet');
const RadioInput = require('common/components/radioInput');
const RadioInputOptionModel = require('common/components/radioInput/radioInputOptionModel');

require('./paymentInfoForm.less');

const billingAddressOptions = [
	new RadioInputOptionModel(true, resources['payment.billing.option.same']),
	new RadioInputOptionModel(false, resources['payment.billing.option.different'])
];

class PaymentInfoForm extends Component {
	constructor(props, context, updater) {
		super(props, context, updater);

		this.state = {
			isBillingAddressEditorVisible: true
		};

		this.onAddressOptionChange = this.onAddressOptionChange.bind(this);
		this.onCountryChange = this.onCountryChange.bind(this);
	}

	componentWillMount() {
		this.setState({
			isBillingAddressEditorVisible: this.props.isBillingAddressEditorVisible
		});
	}

	componentWillReceiveProps(nextProps) {
		const changeSubmittingState = this.props.submitting && !nextProps.submitting;
		if (changeSubmittingState && nextProps.submitFailed) {
			nextProps.onSubmitError();
		}
	}

	onCountryChange() {
		this.props.changeReduxFormField('paymentInfoForm', 'billingAddress.state', null);
	}

	onAddressOptionChange(newOption) {
		this.setState({
			isBillingAddressEditorVisible: !newOption.value
		});
		const addressModel = new AddressModel();
		const fieldsToUntouch = Object.keys(addressModel).map(key => `billingAddress.${key}`);

		if (newOption.value) {
			this.props.changeReduxFormField('paymentInfoForm', 'billingAddress', null);
			this.props.untouchReduxFormField('paymentInfoForm', ...fieldsToUntouch);
		} else {
			this.props.changeReduxFormField('paymentInfoForm', 'billingAddress', addressModel);
			this.props.untouchReduxFormField('paymentInfoForm', ...fieldsToUntouch);
		}
	}

	renderBillingAddressEditor() {
		const { isBillingAddressEditorVisible } = this.state;

		return (
			<div className="payment-info-fieldset__billing-address-container" style={{ display: isBillingAddressEditorVisible ? 'block' : 'none' }}>
				<FormSection name="billingAddress">
					<AddressFieldSet config={this.props.config.billingAddressConfig} onCountryChange={this.onCountryChange} />
				</FormSection>
			</div>
		);
	}

	render() {
		const { isBillingAddressEditorVisible } = this.state;
		const billingAddressEditorContainer = this.renderBillingAddressEditor();
		const selectedValue = billingAddressOptions.find(option => option.value === !isBillingAddressEditorVisible);
		const formTitle = resources['checkoutEditor.paymentTitle'];

		return (
			<form className="payment-info-form">
				<div className="payment-info-form__inner-container">
					<fieldset className="payment-info-form">
						<div className="payment-info-form__inner-container">
							<div className="payment-info-form__title-container">
								<h2 className="payment-info-form__title">{formTitle}</h2>
							</div>
							<div className="payment-info-form__card-info-container">
								<FormSection name="cardInfo">
									<CardInfoFieldSet title={resources['payment.labels.card']} config={this.props.config} />
								</FormSection>
							</div>

							<div className="payment-info-form__billing-address-selector">
								<div className="payment-info-form__label-container">
									<div className="payment-info-form__label">{resources['payment.labels.billing']}</div>
								</div>

								<RadioInput
									options={billingAddressOptions}
									className="payment-info-form__address-options"
									inputClassName="payment-info-form__address-option"
									onChange={this.onAddressOptionChange}
									name="isBillingEqualShipping"
									value={selectedValue}
								/>
							</div>

							{billingAddressEditorContainer}
						</div>
					</fieldset>
				</div>
			</form>
		);
	}
}

PaymentInfoForm.propTypes = {
	onSubmit: PropTypes.func.isRequired, // eslint-disable-line react/no-unused-prop-types
	onSubmitError: PropTypes.func.isRequired,
	config: PropTypes.instanceOf(PaymentConfigModel),
	isBillingAddressEditorVisible: PropTypes.bool.isRequired,
	changeReduxFormField: PropTypes.func.isRequired,
	untouchReduxFormField: PropTypes.func.isRequired,
	submitting: PropTypes.bool,
	submitFailed: PropTypes.bool
};

PaymentInfoForm = reduxForm({
	form: 'paymentInfoForm',
	validate: validateByScheme.bind(null, paymentInfoValidationScheme)
})(PaymentInfoForm);

module.exports = PaymentInfoForm;

