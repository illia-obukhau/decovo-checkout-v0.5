const React = require('react');

const { PropTypes } = require('prop-types');
const classnames = require('classnames');
require('./orderPriceItem.less');

function OrderPriceItem(props) {
	const classNames = classnames('order-price-item', {
		'order-price-item--total': props.isTotal,
		'order-price-item--discount': props.isReducing
	});
	const value = props.isReducing ? `-${props.value}` : props.value;

	return (
		<div className={classNames}>
			<div className="order-price-item__inner-container">
				<div className="order-price-item__label-container">
					<div className="order-price-item__label">{props.label}</div>
				</div>
				<div className="order-price-item__value-container">
					<div className="order-price-item__value">{value}</div>
				</div>
			</div>
		</div>
	);
}

OrderPriceItem.propTypes = {
	label: PropTypes.string.isRequired,
	value: PropTypes.string.isRequired,
	isTotal: PropTypes.bool,
	isReducing: PropTypes.bool
};

module.exports = OrderPriceItem;

