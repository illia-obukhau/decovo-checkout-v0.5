const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');
const currencyHelper = require('common/helpers/currencyHelper');
const currencyFormats = require('common/constants/currencyFormats');

const OrderModel = require('checkout/domain/models/orderModel');
require('./orderTotalPrice.less');

const OrderPriceItem = require('./orderPriceItem');
const PromoCodeInput = require('checkout/domain/components/promoCodeInput');
const OperationLoader = require('common/components/operationLoader');

const resources = require('checkout/common/constants/resources');

class OrderTotalPrice extends Component {
	constructor(props, context, updater) {
		super(props, context, updater);

		this.state = {
			isUpdating: false
		};

		this.onPromoCodeApply = this.onPromoCodeApply.bind(this);
		this.onPromoCodeRemove = this.onPromoCodeRemove.bind(this);
	}

	onPromoCodeApply(promoCode) {
		const { onPromoCodeApply, isReadOnly } = this.props;
		if (!isReadOnly && onPromoCodeApply) {
			this.setUpdating(true);
			onPromoCodeApply(promoCode)
				.then(() => {
					this.setUpdating(false);
				});
		}
	}

	onPromoCodeRemove() {
		const { model, onDiscountsChange, isReadOnly } = this.props;
		if (!isReadOnly && onDiscountsChange) {
			const newDiscounts = model.discounts.filter(discount => !(discount.meta && discount.meta.promoCode));

			onDiscountsChange(newDiscounts);
		}
	}

	setUpdating(value) {
		this.setState({
			isUpdating: value
		});
	}

	renderDiscountItems() {
		const { model } = this.props;

		const rawTotalPrice = model.getRawTotalPrice();
		const discountPriceItems = model.discounts.map((discount) => {
			return {
				label: `${discount.meta.promoCode} PROMOTION CODE`,
				value: currencyHelper.convertFromCentsToDollars(model.getDiscountPrice(rawTotalPrice, discount), currencyFormats.currencyFormat)
			};
		});

		return discountPriceItems.map((entry, index) => {
			return (
				<div className="order-price-item__item-container" key={`price-item-${index}`}>
					<OrderPriceItem label={entry.label} value={entry.value} isReducing={true} />
				</div>
			);
		});
	}

	renderPromoCodeInput() {
		const { model, isReadOnly } = this.props;
		const promoDiscountModel = model.discounts.find(discount => discount.meta && discount.meta.promoCode);

		if (!isReadOnly) {
			return (
				<div className="order-total-price__promo-code-editor-container">
					<PromoCodeInput
						discountModel={promoDiscountModel}
						onApplyClick={this.onPromoCodeApply}
						onRemoveClick={this.onPromoCodeRemove}
					/>
				</div>
			);
		}

		return null;
	}

	render() {
		const { model } = this.props;
		const subTotalPrice = currencyHelper.convertFromCentsToDollars(model.getSubtotalPrice(), currencyFormats.currencyFormat);
		const shippingPrice = currencyHelper.convertFromCentsToDollars(model.getShippingPrice(), currencyFormats.currencyFormat);
		const taxPrice = currencyHelper.convertFromCentsToDollars(model.getTaxPrice(), currencyFormats.currencyFormat);
		const totalPrice = currencyHelper.convertFromCentsToDollars(model.getTotalPrice(), currencyFormats.currencyFormat);
		const discountPriceItems = this.renderDiscountItems();
		const promoCodeInputContainer = this.renderPromoCodeInput();

		return (
			<div className="order-total-price">
				<div className="order-total-price__inner-container">
					<OperationLoader
						isVisible={this.state.isUpdating}
						message={resources['orderTotalPrice.operations.UPDATE']}
						messageClass="order-total-price__processing-message"
					>
						<div className="order-total-price__title-container">
							<h2 className="order-total-price__title">{resources['orderTotalPrice.title']}</h2>
						</div>

						{promoCodeInputContainer}

						<div className="order-total-price__items-list">
							<div className="order-price-item__item-container order-price-item__item-container--total">
								<OrderPriceItem label={resources['orderTotalPrice.subtotal']} value={subTotalPrice} />
							</div>
							<div className="order-price-item__item-container order-price-item__item-container--total">
								<OrderPriceItem label={resources['orderTotalPrice.shipping']} value={shippingPrice} />
							</div>
							<div className="order-price-item__item-container order-price-item__item-container--total">
								<OrderPriceItem label={resources['orderTotalPrice.tax']} value={taxPrice} />
							</div>

							{discountPriceItems}

							<div className="order-price-item__item-container order-price-item__item-container--total">
								<OrderPriceItem label={resources['orderTotalPrice.total']} value={totalPrice} isTotal={true} />
							</div>
						</div>
					</OperationLoader>
				</div>
			</div>
		);
	}
}

OrderTotalPrice.propTypes = {
	isReadOnly: PropTypes.bool,
	model: PropTypes.instanceOf(OrderModel),
	onPromoCodeApply: PropTypes.func,
	onDiscountsChange: PropTypes.func
};

module.exports = OrderTotalPrice;

