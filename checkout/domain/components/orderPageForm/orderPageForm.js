const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');
const classnames = require('classnames');
const autoMapper = require('checkout/common/helpers/autoMapper');
const ContactInfoModel = require('checkout/domain/models/contactInfoModel');
const PaymentInfoModel = require('checkout/domain/models/paymentInfoModel');
const OrderModel = require('checkout/domain/models/orderModel');
const VendorConfigModel = require('checkout/domain/models/vendorConfigModel');
const ShoppingBagModel = require('checkout/domain/models/shoppingBagModel');

const resources = require('checkout/common/constants/resources');
const ContactInfoForm = require('checkout/domain/components/contactInfoForm');
const PaymentInfoForm = require('checkout/domain/components/paymentInfoForm');

const ShoppingBagEditor = require('checkout/domain/components/shoppingBagEditor');
const OrderTotalPrice = require('checkout/domain/components/orderTotalPrice');
const OperationLoader = require('common/components/operationLoader');

require('./orderPageForm.less');

class OrderPageForm extends Component {
	constructor(props, context, updater) {
		super(props, context, updater);

		this.state = {
			isSubmitting: false,
			isContactFormSubmitting: false,
			isPaymentFormSubmitting: false
		};

		this.onSubmitClick = this.onSubmitClick.bind(this);
		this.onContactInfoSubmit = this.onContactInfoSubmit.bind(this);
		this.onPaymentInfoSubmit = this.onPaymentInfoSubmit.bind(this);
		this.onContactInfoSubmitError = this.onContactInfoSubmitError.bind(this);
		this.onPaymentInfoSubmitError = this.onPaymentInfoSubmitError.bind(this);
		this.onPartSubmit = this.onPartSubmit.bind(this);
	}

	componentDidMount() {
		const { model: { contactInfo, paymentInfo } } = this.props;
		this.props.initializeReduxForm('contactInfoForm', contactInfo);
		this.props.initializeReduxForm('paymentInfoForm', paymentInfo);
	}

	onSubmitClick() {
		this.setState({
			isContactFormSubmitting: true,
			isPaymentFormSubmitting: true
		}, () => {
			this.props.submitReduxForm('contactInfoForm');
			this.props.submitReduxForm('paymentInfoForm');
		});
	}

	onPartSubmit() {
		const { isContactFormSubmitting, isPaymentFormSubmitting } = this.state;
		if (!isContactFormSubmitting && !isPaymentFormSubmitting) {
			this.setState({
				isSubmitting: true
			}, () => {
				this.props.onSubmit()
					.then(() => {
						this.setState({
							isSubmitting: false
						});
					});
			});
		}
	}

	onContactInfoSubmitError() {
		this.setState({
			isContactFormSubmitting: false
		});
	}

	onPaymentInfoSubmitError() {
		this.setState({
			isPaymentFormSubmitting: false
		});
	}

	onContactInfoSubmit(formValues) {
		const contactInfo = autoMapper.map('ContactInfoStateModel', ContactInfoModel, formValues);

		return this.props.onContactInfoChange(contactInfo)
			.then(() => {
				this.setState({
					isContactFormSubmitting: false
				}, this.onPartSubmit);
			});
	}

	onPaymentInfoSubmit(formValues) {
		const paymentInfo = autoMapper.map('PaymentInfoStateModel', PaymentInfoModel, formValues);

		return this.props.onPaymentInfoChange(paymentInfo)
			.then(() => {
				this.setState({
					isPaymentFormSubmitting: false
				}, this.onPartSubmit);
			});
	}

	render() {
		const {
			model,
			shoppingBag,
			isBillingAddressEditorVisible,
			onShoppingBagChange,
			onPromoCodeApply,
			onDiscountsChange,
			changeReduxFormField,
			untouchReduxFormField,
			vendorConfig
		} = this.props;
		const { isSubmitting } = this.state;
		const submitButtonClassNames = classnames('order-page-form__submit', 'button', 'button--full', {
			'button--loading': isSubmitting
		});

		return (
			<div className="order-page-form">
				<div className="order-page-form__inner-container">
					<div className="order-page-form__form-container">
						<OperationLoader isVisible={this.state.isSubmitting}>
							<div className="columns-layout">
								<div
									className="order-page-form__column-container columns-layout__column
						columns-layout__column--lg-50 columns-layout__column--md-50">
									<div className="order-page-form__contact-info-container">
										<ContactInfoForm
											onSubmit={this.onContactInfoSubmit}
											onSubmitError={this.onContactInfoSubmitError}
											config={vendorConfig.contactConfig}
											changeReduxFormField={changeReduxFormField}
										/>
									</div>
									<div className="order-page-form__payment-info-container">
										<PaymentInfoForm
											config={vendorConfig.paymentConfig}
											changeReduxFormField={changeReduxFormField}
											untouchReduxFormField={untouchReduxFormField}
											onSubmit={this.onPaymentInfoSubmit}
											onSubmitError={this.onPaymentInfoSubmitError}
											isBillingAddressEditorVisible={isBillingAddressEditorVisible}
										/>
									</div>
								</div>
								<div
									className="order-page-form__column-container
							columns-layout__column
							columns-layout__column--lg-50
							columns-layout__column--md-50">
									<div className="order-page-form__shopping-bag-container">
										<ShoppingBagEditor
											model={shoppingBag}
											config={vendorConfig.shoppingBagConfig}
											onShoppingBagChange={onShoppingBagChange}
										/>
									</div>
									<div className="order-page-form__order-summary-container">
										<OrderTotalPrice
											model={model}
											onDiscountsChange={onDiscountsChange}
											onPromoCodeApply={onPromoCodeApply}
										/>
									</div>
								</div>
							</div>
						</OperationLoader>
					</div>
					<div className="order-page-form__submit-container">
						<button className={submitButtonClassNames} onClick={this.onSubmitClick} disabled={isSubmitting}>
							{resources['checkoutEditor.submit']}
							<img className="button__loader" src="checkout/static/images/spinner.gif" alt="" />
						</button>
					</div>
				</div>
			</div>
		);
	}
}

OrderPageForm.propTypes = {
	onSubmit: PropTypes.func.isRequired,
	model: PropTypes.instanceOf(OrderModel),
	vendorConfig: PropTypes.instanceOf(VendorConfigModel),
	shoppingBag: PropTypes.instanceOf(ShoppingBagModel),
	onPromoCodeApply: PropTypes.func.isRequired,

	onContactInfoChange: PropTypes.func.isRequired,
	onPaymentInfoChange: PropTypes.func.isRequired,
	onShoppingBagChange: PropTypes.func.isRequired,
	onDiscountsChange: PropTypes.func.isRequired,
	isBillingAddressEditorVisible: PropTypes.bool.isRequired,
	initializeReduxForm: PropTypes.func.isRequired,
	submitReduxForm: PropTypes.func.isRequired,
	changeReduxFormField: PropTypes.func.isRequired,
	untouchReduxFormField: PropTypes.func.isRequired
};

module.exports = OrderPageForm;

