const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');
const { reduxForm } = require('redux-form/immutable');
const { Field, FormSection } = require('redux-form');

const ContactConfigModel = require('checkout/domain/models/contactConfigModel');

const TextField = require('checkout/common/components/textField');
const AddressFieldSet = require('checkout/domain/components/addressFieldSet');
const MaskedTextField = require('checkout/common/components/maskedTextField');
const resources = require('checkout/common/constants/resources');
const validateByScheme = require('checkout/features/validation/validateByScheme');
const contactInfoValidationScheme = require('checkout/domain/validationSchemes/contactInfoValidationScheme');

require('./contactInfoForm.less');

class ContactInfoForm extends Component {
	componentWillReceiveProps(nextProps) {
		const changeSubmittingState = this.props.submitting && !nextProps.submitting;
		if (changeSubmittingState && nextProps.submitFailed) {
			nextProps.onSubmitError();
		}
	}

	onCountryChange() {
		this.props.changeReduxFormField('contactInfoForm', 'shippingAddress.state', null);
	}

	render() {
		const formTitle = resources['checkoutEditor.contactTitle'];

		return (
			<form className="contact-info-form" >
				<div className="contact-info-form__inner-container">
					<fieldset className="contact-info-form">
						<div className="contact-info-form__inner-container">
							<div className="contact-info-form__title-container">
								<h2 className="contact-info-form__title">{formTitle}</h2>
							</div>
							<div className="contact-info-form__input-container">
								<div className="contact-info-form__label">{resources['contact.labels.email']}</div>
								<Field
									className="contact-info-form__input"
									name="email"
									component={TextField}
									placeholder={resources['contact.placeholders.email']}
								/>
							</div>
							<div className="contact-info-form__block-container">
								<FormSection name="shippingAddress">
									<AddressFieldSet
										config={this.props.config.shippingAddressConfig}
										onCountryChange={this.onCountryChange.bind(this)}
										title={resources['contact.labels.shippingAddress']}
									/>
								</FormSection>
							</div>
							<div className="contact-info-form__input-container">
								<Field
									className="contact-info-form__input"
									name="phone"
									component={MaskedTextField}
									mask="(111)-111-1111"
									placeholder={resources['contact.placeholders.phone']}
								/>
							</div>
						</div>
					</fieldset>
				</div>
			</form>
		);
	}
}

ContactInfoForm.propTypes = {
	onSubmit: PropTypes.func.isRequired, // eslint-disable-line react/no-unused-prop-types,
	onSubmitError: PropTypes.func.isRequired,
	config: PropTypes.instanceOf(ContactConfigModel),
	changeReduxFormField: PropTypes.func.isRequired,
	submitting: PropTypes.bool,
	submitFailed: PropTypes.bool
};

ContactInfoForm = reduxForm({
	form: 'contactInfoForm',
	validate: validateByScheme.bind(null, contactInfoValidationScheme)
})(ContactInfoForm);

module.exports = ContactInfoForm;

