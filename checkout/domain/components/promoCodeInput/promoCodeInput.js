const React = require('react');
const classnames = require('classnames');

const { Component } = React;
const { PropTypes } = require('prop-types');
const DiscountModel = require('checkout/domain/models/discountModel');

const templateHelper = require('common/helpers/templateHelper');
const resources = require('checkout/common/constants/resources');

const Input = require('common/components/input');
require('./promoCodeInput.less');

class PromoCodeInput extends Component {
	constructor(props, context, updater) {
		super(props, context, updater);

		this.state = {
			isEditMode: false,
			promoCode: ''
		};

		this.onApplyClick = this.onApplyClick.bind(this);
		this.onRemoveClick = this.onRemoveClick.bind(this);
		this.onInputChange = this.onInputChange.bind(this);
		this.onSwitchModeClick = this.onSwitchModeClick.bind(this);
	}

	onInputChange(event) {
		this.setState({
			promoCode: event.target.value
		});
	}

	onApplyClick() {
		const { onApplyClick } = this.props;
		const { promoCode } = this.state;

		if (promoCode) {
			onApplyClick(promoCode);
		}
		this.setState({
			isEditMode: false
		});
	}

	onRemoveClick() {
		this.props.onRemoveClick();
		this.setState({
			promoCode: ''
		});
	}

	onSwitchModeClick() {
		const { discountModel } = this.props;

		if (discountModel) {
			this.onRemoveClick();
		} else {
			this.setState({
				isEditMode: !this.state.isEditMode
			}, () => {
				this.input.setFocus();
			});
		}
	}

	renderInput() {
		return (
			<div className="promo-code-input__input-container">
				<div className="promo-code-input__input-wrapper">
					<Input
						ref={(input) => {
							this.input = input;
						}}
						className="promo-code-input__input"
						onChange={this.onInputChange}
						value={this.state.promoCode}
					/>
				</div>
				<div className="promo-code-input__link-wrapper">
					<span className="promo-code-input__link" onClick={this.onApplyClick}>Apply</span>
				</div>
			</div>
		);
	}

	renderModeControls() {
		const { discountModel } = this.props;
		const linkText = discountModel ?
			resources['orderTotalPrice.removeBtnText'] :
			resources['orderTotalPrice.addBtnText'];

		return (
			<div className="promo-code-input__controls-container">
				<span className="promo-code-input__link" onClick={this.onSwitchModeClick}>{linkText}</span>
			</div>
		);
	}

	render() {
		const { discountModel } = this.props;
		const { isEditMode, promoCode } = this.state;
		const inputContainer = isEditMode ? this.renderInput() : null;
		const modeControlsContainer = !isEditMode ? this.renderModeControls() : null;
		const labelText = discountModel ?
			resources['orderTotalPrice.giftCodeAppliedMessage'] :
			resources['orderTotalPrice.giftCodeMessage'];

		const rootClass = classnames('promo-code-input', {
			'promo-code-input--edit-mode': isEditMode,
			'promo-code-input--applied': !!discountModel
		});

		return (
			<div className={rootClass}>
				<div className="promo-code-input__inner-container">
					<div className="promo-code-input__label-container">
						<span className="promo-code-input__label">{templateHelper.format(labelText, { promoCode })}</span>
					</div>
					{modeControlsContainer}
					{inputContainer}
				</div>
			</div>
		);
	}
}

PromoCodeInput.propTypes = {
	discountModel: PropTypes.instanceOf(DiscountModel),
	onApplyClick: PropTypes.func.isRequired,
	onRemoveClick: PropTypes.func.isRequired
};

module.exports = PromoCodeInput;
