const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');
const resources = require('checkout/common/constants/resources');
require('./cardInfoFieldSet.less');
const paymentFormHelper = require('checkout/features/payment/helpers/paymentFormHelper');

const { Field } = require('redux-form');
const CreditCardField = require('checkout/common/components/creditCardField');
const PaymentTypeItem = require('checkout/features/payment/components/paymentTypeItem');
const TextField = require('checkout/common/components/textField');
const SelectBoxField = require('checkout/common/components/selectBoxField');
const SelectBoxOptionModel = require('checkout/common/components/selectBoxField/selectBoxOptionModel');
const PaymentConfigModel = require('checkout/domain/models/paymentConfigModel');

class CardInfoFieldSet extends Component {
	renderInternalPayments() {
		const paymentTypesContainer = this.props.config.allowedPaymentTypes
			.filter(paymentType => paymentType.isInternal)
			.map((paymentItem, index) => {
				return (
					<div className="card-info-fieldset__card-type-container" key={`internal-payment-${index}`}>
						<PaymentTypeItem brandImgUrl={paymentItem.brandImgUrl} />
					</div>
				);
			});

		return (
			<div className="card-info-fieldset__card-types-container">
				{paymentTypesContainer}
			</div>
		);
	}

	render() {
		const monthOptions = paymentFormHelper.getExpirationMonthOptions();
		const yearOptions = paymentFormHelper.getExpirationYearOptions();

		return (
			<fieldset className="card-info-fieldset">
				<div className="card-info-fieldset__inner-container">

					<div className="card-info-fieldset__header-container">
						<div className="card-info-fieldset__description-container">
							<div className="card-info-fieldset__description">{this.props.title}</div>
						</div>
						<div className="card-info-fieldset__note-container">
							<div className="card-info-fieldset__note">{resources['payment.text.security']}</div>
						</div>
					</div>

					<div className="columns-layout columns-layout--reverse-wrap">
						<div className="card-info-fieldset__column columns-layout__column columns-layout__column--lg-66">
							<div className="card-info-fieldset__input-container">
								<Field
									className="card-info-fieldset__input"
									name="cardNumber"
									component={CreditCardField}
									placeholder={resources['payment.placeholders.cardNumber']}
								/>
							</div>
						</div>
						<div className="card-info-fieldset__column columns-layout__column columns-layout__column--lg-33">
							{this.renderInternalPayments()}
						</div>
					</div>

					<div className="card-info-fieldset__input-container">
						<Field
							className="card-info-fieldset__input"
							name="cardHolder"
							component={TextField}
							placeholder={resources['payment.placeholders.cardHolder']}
						/>
					</div>

					<div className="columns-layout">
						<div className="card-info-fieldset__column columns-layout__column columns-layout__column--lg-33">
							<div className="card-info-fieldset__input-container">
								<Field
									className="card-info-fieldset__input"
									name="expirationMonth"
									component={SelectBoxField}
									options={monthOptions}
									defaultOption={new SelectBoxOptionModel('', resources['payment.placeholders.expirationMonth'])}
								/>
							</div>
						</div>
						<div className="card-info-fieldset__column columns-layout__column columns-layout__column--lg-33">
							<div className="card-info-fieldset__input-container">
								<Field
									className="card-info-fieldset__input"
									name="expirationYear"
									component={SelectBoxField}
									options={yearOptions}
									defaultOption={new SelectBoxOptionModel('', resources['payment.placeholders.expirationYear'])}
								/>
							</div>
						</div>
						<div className="card-info-fieldset__column columns-layout__column columns-layout__column--lg-33">
							<div className="card-info-fieldset__input-container">
								<Field
									className="card-info-fieldset__input"
									name="cvv"
									component={TextField}
									placeholder={resources['payment.placeholders.cvv']}
								/>
							</div>
						</div>
					</div>

				</div>
			</fieldset>
		);
	}
}

CardInfoFieldSet.propTypes = {
	title: PropTypes.string.isRequired,
	config: PropTypes.instanceOf(PaymentConfigModel)
};

module.exports = CardInfoFieldSet;
