const React = require('react');

const { PropTypes } = require('prop-types');

require('./orderProperty.less');

function OrderProperty(props) {
	return (
		<div className="order-property">
			<div className="order-property__inner-container">
				<div className="order-property__label-container">
					<div className="order-property__label">{props.label}</div>
				</div>
				<div className="order-property__value-container">
					<div className="order-property__value">{props.children}</div>
				</div>
			</div>
		</div>
	);
}

OrderProperty.propTypes = {
	label: PropTypes.string,
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.element),
		PropTypes.element
	])
};

module.exports = OrderProperty;

