const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');

require('./orderDetails.less');
const isEqual = require('lodash/isEqual');

const resources = require('checkout/common/constants/resources');
const templateHelper = require('common/helpers/templateHelper');
const Phone = require('common/components/phone');
const MapPanel = require('checkout/common/components/mapPanel');
const phoneFormats = require('common/constants/phoneFormats');

const OrderProperty = require('./orderProperty');

const ShoppingBagEditor = require('checkout/domain/components/shoppingBagEditor');
const OrderTotalPrice = require('checkout/domain/components/orderTotalPrice');
const OrderModel = require('checkout/domain/models/orderModel');
const VendorConfigModel = require('checkout/domain/models/vendorConfigModel');

const nyPosition = [40.730610, -73.935242];

class OrderDetails extends Component {
	constructor(props, context, updater) {
		super(props, context, updater);

		this.state = {
			markerPosition: nyPosition
		};
	}

	componentWillReceiveProps(nextProps) {
		const oldShippingAddress = this.props.model.contactInfo.shippingAddress;
		const nextShippingAddress = nextProps.model.contactInfo.shippingAddress;

		if (!isEqual(oldShippingAddress, nextShippingAddress)) {
			this.props.onGetCoordinatesByAddress(nextShippingAddress.getAddressLine())
				.then((coordinates) => {
					this.setState({
						markerPosition: coordinates
					});
				});
		}
	}

	renderAddressProperty(address, label) {
		if (address) {
			const fullName = [address.firstName, address.lastName].join(' ');
			const fullAddress = address.getAddressLine();

			return (
				<OrderProperty label={label}>
					<div>{fullName}</div>
					<div>{fullAddress}</div>
				</OrderProperty>
			);
		}

		return null;
	}

	renderPhoneProperty(phone, label) {
		if (phone) {
			return (
				<OrderProperty label={label}>
					<Phone value={phone} format={phoneFormats.dottedNational} />
				</OrderProperty>
			);
		}

		return null;
	}

	renderPaymentInfoProperty(paymentInfo, label) {
		if (paymentInfo) {
			return (
				<OrderProperty label={label}>
					<div>{paymentInfo.cardInfo.cardHolder}</div>
					<div>{paymentInfo.cardInfo.cardNumber}</div>
				</OrderProperty>
			);
		}

		return null;
	}

	render() {
		const { model, config } = this.props;
		const { markerPosition } = this.state;
		const title = templateHelper.format('Order number #{orderId}', { orderId: model.id });
		const firstName = model && model.contactInfo && model.contactInfo.shippingAddress ? model.contactInfo.shippingAddress.firstName : null;
		const orderMessageTitle =
			templateHelper.format(resources['orderDetails.messageTitle'], { name: firstName });
		const orderMessage = templateHelper.format(resources['orderDetails.message'], { email: model.contactInfo.email });

		return (
			<div className="order-details">
				<div className="order-details__inner-container columns-layout">
					<div className="columns-layout__column columns-layout__column--lg-50">
						<div className="order-details__title-container">
							<h2 className="order-details__title">{title}</h2>
						</div>
						<div className="order-details__confirmation-title-container">
							<div className="order-details__confirmation-title">{orderMessageTitle}</div>
						</div>
						<div className="order-details__message-container">
							<div className="order-details__message">{orderMessage}</div>
						</div>
						<div className="order-details__map-container">
							<MapPanel markerPosition={markerPosition} zoomLevel={8} />
						</div>
						<div className="order-details__properties">
							<div className="order-details__property-container">
								{this.renderAddressProperty(model.contactInfo.shippingAddress, 'Shipping Info')}
							</div>

							<div className="order-details__property-container">
								{this.renderPhoneProperty(model.contactInfo.phone)}
							</div>

							<div className="order-details__property-container">
								{this.renderAddressProperty(model.paymentInfo.billingAddress, 'Billing Info')}
							</div>

							<div className="order-details__property-container">
								{this.renderPaymentInfoProperty(model.paymentInfo, 'Payment Info')}
							</div>
						</div>
					</div>

					<div className="columns-layout__column columns-layout__column--lg-50">
						<div className="order-details__shopping-bag-container">
							<ShoppingBagEditor model={model.shoppingBag} isReadOnly={true} config={config.shoppingBagConfig} />
						</div>
						<div className="order-details__order-summary-container">
							<OrderTotalPrice model={model} isReadOnly={true} />
						</div>
					</div>
				</div>

				<div className="order-details__controls-container columns-layout columns-layout--reverse-wrap">
					<div className="columns-layout__column columns-layout__column--lg-50">
						<div className="order-details__contact-us-message">
							{resources['orderDetails.helpMessage']}
							<a className="order-details__help-link">{resources['orderDetails.helpLink']}</a>
						</div>
					</div>
					<div className="columns-layout__column columns-layout__column--lg-50">
						<div className="order-details__continue-container">
							<button
								className="order-details__continue-button button button--full"
								onClick={this.props.onContinueShoppingClick}
							>
								{resources['orderDetails.continueShopping']}
							</button>
						</div>
						<div className="order-details__print-link-container">
							<a className="order-details__print-link" onClick={this.props.onContinueShoppingClick}>
								{resources['orderDetails.print']}
							</a>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

OrderDetails.propTypes = {
	model: PropTypes.instanceOf(OrderModel),
	config: PropTypes.instanceOf(VendorConfigModel),
	onContinueShoppingClick: PropTypes.func.isRequired,
	onGetCoordinatesByAddress: PropTypes.func.isRequired
};

module.exports = OrderDetails;

