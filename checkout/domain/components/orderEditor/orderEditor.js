const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');

const OrderModel = require('checkout/domain/models/orderModel');
const ShoppingBagModel = require('checkout/domain/models/shoppingBagModel');
const VendorConfigModel = require('checkout/domain/models/vendorConfigModel');
const orderStatuses = require('checkout/domain/constants/orderStatuses');
require('./orderEditor.less');

const OrderPageForm = require('checkout/domain/components/orderPageForm');

class OrderEditor extends Component {
	constructor(props, context, updater) {
		super(props, context, updater);

		this.state = {
			model: new OrderModel(null, null, null, [], null, orderStatuses.DRAFT)
		};

		this.onSubmit = this.onSubmit.bind(this);
		this.onShoppingBagChange = this.onShoppingBagChange.bind(this);
		this.onPromoCodeApply = this.onPromoCodeApply.bind(this);
	}

	componentWillMount() {
		const orderModel = Object.assign(new OrderModel(), this.props.model);
		orderModel.shoppingBag = this.props.shoppingBag;

		this.onUpdateStateModel(orderModel);
	}

	onUpdateStateModel(model) {
		let newModel = model;
		if (!model) {
			newModel = new OrderModel(null, null, null, [], null, orderStatuses.DRAFT);
		}

		this.setState({
			model: newModel
		});
	}

	onUpdateField(fieldName, value) {
		return new Promise((resolve, reject) => {
			const model = Object.assign(new OrderModel(), this.state.model);
			model[fieldName] = value;

			this.setState({ model }, () => {
				resolve(model);
			});
		});
	}

	onShoppingBagChange(newShoppingBag) {
		return this.props.onShoppingBagChange(newShoppingBag)
			.then((updatedShoppingBag) => {
				return this.onUpdateField('shoppingBag', updatedShoppingBag);
			});
	}

	onPromoCodeApply(promoCode) {
		return this.props.onPromoCodeApply(promoCode)
			.then((discount) => {
				const newDiscounts = this.state.model.discounts.slice(0);
				newDiscounts.push(discount);

				return this.onUpdateField('discounts', newDiscounts);
			});
	}

	onSubmit() {
		const newOrder = Object.assign(new OrderModel(), this.state.model);
		if (!newOrder.paymentInfo.billingAddress) {
			newOrder.paymentInfo.billingAddress = newOrder.contactInfo.shippingAddress;
		}

		return this.props.onOrderSubmit(newOrder);
	}

	render() {
		const { model, model: { contactInfo, paymentInfo } } = this.state;
		const {
			shoppingBag,
			vendorConfig,
			initializeReduxForm,
			submitReduxForm,
			changeReduxFormField,
			untouchReduxFormField
		} = this.props;
		const isShippingAddressSet = !!(contactInfo && contactInfo.shippingAddress);
		const isBillingAddressSet = !!(paymentInfo && paymentInfo.billingAddress);
		const isBillingAddressEditorVisible = isShippingAddressSet &&
			isBillingAddressSet && !contactInfo.shippingAddress.isEqual(paymentInfo.billingAddress);

		return (
			<div className="order-editor">
				<div className="order-editor__inner-container">
					<OrderPageForm
						model={model}
						shoppingBag={shoppingBag}
						vendorConfig={vendorConfig}
						onSubmit={this.onSubmit}
						onContactInfoChange={this.onUpdateField.bind(this, 'contactInfo')}
						onDiscountsChange={this.onUpdateField.bind(this, 'discounts')}
						onPaymentInfoChange={this.onUpdateField.bind(this, 'paymentInfo')}
						onShoppingBagChange={this.onShoppingBagChange}
						onPromoCodeApply={this.onPromoCodeApply}
						isBillingAddressEditorVisible={isBillingAddressEditorVisible}
						initializeReduxForm={initializeReduxForm}
						submitReduxForm={submitReduxForm}
						changeReduxFormField={changeReduxFormField}
						untouchReduxFormField={untouchReduxFormField}
					/>
				</div>
			</div>
		);
	}
}

OrderEditor.propTypes = {
	model: PropTypes.instanceOf(OrderModel),
	shoppingBag: PropTypes.instanceOf(ShoppingBagModel),
	vendorConfig: PropTypes.instanceOf(VendorConfigModel),
	onOrderSubmit: PropTypes.func.isRequired,
	onPromoCodeApply: PropTypes.func.isRequired,
	onShoppingBagChange: PropTypes.func.isRequired,
	initializeReduxForm: PropTypes.func.isRequired,
	submitReduxForm: PropTypes.func.isRequired,
	changeReduxFormField: PropTypes.func.isRequired,
	untouchReduxFormField: PropTypes.func.isRequired
};

module.exports = OrderEditor;

