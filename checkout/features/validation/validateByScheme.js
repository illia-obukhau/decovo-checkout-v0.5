/* eslint-disable consistent-return, array-callback-return */
const existingFieldRules = require('checkout/features/validation/fieldRules');

function getValidationFunc(entitiesToRulesMap, validateFunc) {
	return (valuesRoot) => {
		const validationContext = {};

		const validatableEntityNames = Object.keys(entitiesToRulesMap);
		validatableEntityNames.forEach((validatableEntityName) => {
			const entityToRulesMap = entitiesToRulesMap[validatableEntityName];

			validationContext[validatableEntityName] = validateFunc(valuesRoot[validatableEntityName], entityToRulesMap);
		});

		return validationContext;
	};
}

function validateField(value, fieldToRulesMap) {
	if (fieldToRulesMap && fieldToRulesMap instanceof Object) {
		const validationRuleNames = Object.keys(fieldToRulesMap);

		const validationErrors = validationRuleNames.map((validationRuleName) => {
			const isRuleExists = !!existingFieldRules[validationRuleName];

			if (isRuleExists) {
				const validationRuleOptions = fieldToRulesMap[validationRuleName];

				return existingFieldRules[validationRuleName].apply(null, [value, validationRuleOptions]);
			}
		});

		return validationErrors.find(error => !!error);
	}
}

function validateFieldSet(valuesRoot, validationConfig) {
	let fieldsValidationContext = {};
	let fieldSetsValidationContext = {};
	const values = valuesRoot || {};

	if (!validationConfig.isOptional || valuesRoot) {
		if (validationConfig.fields) {
			fieldsValidationContext = getValidationFunc(validationConfig.fields, validateField)(values);
		}

		if (validationConfig.fieldSets) {
			fieldSetsValidationContext = getValidationFunc(validationConfig.fieldSets, validateFieldSet)(values);
		}
	}

	return Object.assign({}, fieldsValidationContext, fieldSetsValidationContext);
}

module.exports = (validationConfig, values) => {
	return validateFieldSet(values, validationConfig);
};

/* eslint-disable consistent-return, array-callback-return */
