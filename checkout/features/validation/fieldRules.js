const validationRules = require('checkout/features/validation/validationRules');

module.exports = {
	isRequired: validationRules.isEmpty,
	minLength: validationRules.minLength,
	maxLength: validationRules.maxLength,
	isEmail: validationRules.isEmail
};
