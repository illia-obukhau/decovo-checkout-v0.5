/* eslint-disable consistent-return */

const validator = require('validator');

function isEmpty(value, isRequired) {
	if (!isRequired || (value && !validator.isEmpty(value))) {
		return;
	}

	return '* Required';
}

function maxLength(value, max) {
	if (!value || validator.isLength(value, { max })) {
		return;
	}

	return `Max length is - ${max}`;
}

function minLength(value, min) {
	if (!value || validator.isLength(value, { min })) {
		return;
	}

	return `Min length is - ${min}!`;
}

function isEmail(value) {
	if (value && validator.isEmail(value)) {
		return;
	}

	return 'Incorrect email!';
}

function isZip(value) {
	const result = value.match(/(^\d{5}$)|(^\d{5}-\d{4}$)/);
	if (result === null) {
		return 'Incorrect zip!';
	}
}

module.exports = {
	isEmpty,
	maxLength,
	minLength,
	isEmail,
	isZip
};

/* eslint-disable consistent-return */
