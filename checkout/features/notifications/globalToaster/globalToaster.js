const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');
const { Notifs } = require('redux-notifications');

require('./globalToaster.less');

class GlobalToaster extends Component {
	render() {
		const { onClick, transitionEnterTimeout, transitionLeaveTimeout } = this.props;

		return (
			<div className="global-toaster__wrapper" role="alert" onClick={onClick}>
				<Notifs
					componentClassName="global-toaster"
					transitionEnterTimeout={transitionEnterTimeout}
					transitionLeaveTimeout={transitionLeaveTimeout}
				/>
			</div>
		);
	}
}

GlobalToaster.propTypes = {
	onClick: PropTypes.func,
	transitionEnterTimeout: PropTypes.number,
	transitionLeaveTimeout: PropTypes.number
};

GlobalToaster.defaultProps = {
	transitionEnterTimeout: 500,
	transitionLeaveTimeout: 500,
	onClick: null
};

module.exports = GlobalToaster;
