

const notificationActions = require('redux-notifications').actions;
const { bindActionCreators } = require('redux');

const notificationMapping = {
	error: 'danger',
	success: 'success',
	warning: 'warning',
	info: 'info'
};

const defaults = {
	kind: 'info',
	dismissAfter: 3000
};

const notificationMiddleware = store => next => (action) => {
	const notificationService = bindActionCreators(notificationActions, store.dispatch);
	const notification = action.notification || (action.meta && action.meta.notification);
	const notificationConfig = action.notificationConfig || (action.meta && action.meta.notificationConfig);

	if (typeof notification === 'object') {
		Object.keys(notification).forEach((type) => {
			const actionPostfix = type.toUpperCase();
			const isNotificationType = action.type.split('_').indexOf(actionPostfix) !== -1;

			if (isNotificationType) {
				const message = (Error.prototype.isPrototypeOf(action.payload) && action.payload.clientMessage) ?
					action.payload.clientMessage : notification[type];
				const options = Object.assign({}, defaults, {
					message,
					kind: notificationMapping[type]
				});

				notificationService.notifSend(options);
			}
		});
	} else if (typeof notificationConfig === 'object') {
		// TODO: use explicit notifications instead of config overloading.
		const options = Object.assign({}, defaults, {
			message: notificationConfig.message,
			kind: notificationConfig.type,
			dismissAfter: notificationConfig.dismissAfter
		});

		notificationService.notifSend(options);
	}

	return next(action);
};

module.exports = notificationMiddleware;
