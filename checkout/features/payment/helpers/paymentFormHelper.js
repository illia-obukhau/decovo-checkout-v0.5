const moment = require('moment');
const SelectBoxOptionModel = require('checkout/common/components/selectBoxField/selectBoxOptionModel');
const months = require('checkout/features/payment/constants/months');

module.exports.getExpirationYearOptions = (maxValue) => {
	const yearOptions = [];
	const currentYear = moment().year();
	const topBoundary = maxValue || currentYear + 10;
	for (let year = currentYear; year <= topBoundary; year++) {
		yearOptions.push(new SelectBoxOptionModel(year.toString(), year.toString()));
	}

	return yearOptions;
};

module.exports.getExpirationMonthOptions = () => {
	return months.map((month) => {
		return new SelectBoxOptionModel(month.number, month.name);
	});
};
