const React = require('react');
const { PropTypes } = require('prop-types');
const classnames = require('classnames');

const { Component } = React;
require('./paymentTypeItem.less');

require('checkout/static/images/master-card.png');
require('checkout/static/images/visa.png');
require('checkout/static/images/american-express.png');
require('checkout/static/images/discover.png');

class CardTypeItem extends Component {
	render() {
		const { className, isHighlighted, brandImgUrl, onClick } = this.props;

		const cardTypeItemClassName = classnames('card-type-item', className);
		const pillowClassNames = classnames('card-type-item__pillow', {
			'card-type-item__pillow--highlighted': isHighlighted
		});

		return (
			<div className={cardTypeItemClassName} onClick={onClick}>
				<div className="card-type-item__inner-container">
					<span className={pillowClassNames}>
						<img className="card-type-item__image" alt="" src={brandImgUrl} />
					</span>
				</div>
			</div>
		);
	}
}

CardTypeItem.propTypes = {
	isHighlighted: PropTypes.bool,
	brandImgUrl: PropTypes.string,
	className: PropTypes.string,
	onClick: PropTypes.func
};

CardTypeItem.defaultProps = {
	className: ''
};

module.exports = CardTypeItem;
