class CardModel {
	constructor(cardHolder, cardNumber, cardType, cvv, expirationMonth, expirationYear) {
		this.cardHolder = cardHolder;
		this.cardNumber = cardNumber;
		this.cardType = cardType;
		this.cvv = cvv;
		this.expirationMonth = expirationMonth;
		this.expirationYear = expirationYear;
	}
}

module.exports = CardModel;
