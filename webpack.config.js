const path = require('path');

const webpack = require('webpack');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const NODE_ENV = process.env.NODE_ENV;
const nodePath = process.env.NODE_PATH;
const rootPath = nodePath ? nodePath.split(';')[0] : './';

const paths = {
	publicPath: '/',
	assets: 'assets/',
	checkout: {
		in: 'checkout',
		outRelativeToContext: 'checkout',
		out: 'dist',
		outJsName: 'checkout/index',
		favicons: 'static/favicons',
		htmlTemplate: 'index.ejs',
		html: 'index.html',
		indexJs: 'index.js'
	}
};

const defaultConfig = {
	resolve: {
		modules: [path.resolve(rootPath), 'node_modules']
	},
	entry: {
		[paths.checkout.outJsName]: [
			'babel-polyfill',
			path.join(__dirname, paths.checkout.in, paths.checkout.indexJs)
		]
	},
	output: {
		path: path.join(__dirname, paths.checkout.out),
		filename: '[name].js',
		publicPath: paths.publicPath
	},
	module: {
		loaders: [
			{
				test: /\.jsx?$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				query: {
					presets: ['es2015', 'react'],
					plugins: ['transform-react-jsx-img-import']
				}
			},
			{
				test: /\.less$/,
				loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader!less-loader' })
			},
			{
				test: /\.css$/,
				loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader' })
			},
			{
				test: /\.(svg|jpg|png|gif)/,
				loader: 'file-loader',
				options: {
					name: `${paths.assets}[name].[ext]`
				}
			},
			{
				test: /\.(woff|woff2|eot|ttf)/,
				loader: 'file-loader',
				options: {
					name: `${paths.assets}[name].[ext]`
				}
			}
		]
	}
};

const defaultPlugins = [
	new webpack.NormalModuleReplacementPlugin(/^joi$/, 'joi-browser'),
	new CopyWebpackPlugin([
		{
			from: path.join(__dirname, paths.checkout.in, paths.checkout.favicons),
			to: path.join(paths.checkout.outRelativeToContext)
		}
	]),
	new webpack.IgnorePlugin(/locale/, /moment$/),
	new ExtractTextPlugin('[name].css'),
	new HtmlWebpackPlugin({
		template: path.join(__dirname, paths.checkout.in, paths.checkout.htmlTemplate),
		filename: path.join(__dirname, paths.checkout.out, paths.checkout.outRelativeToContext, paths.checkout.html),
		hash: true,
		data: {
			version: '1.0'
		}
	})
];

const developmentConfigFactory = () => {
	return Object.assign({}, defaultConfig, {
		devtool: 'source-map',
		plugins: defaultPlugins.concat([
			new webpack.DefinePlugin({
				'process.env': {
					NODE_ENV: JSON.stringify('development'),
					MAP_SERVICE_API_KEY: JSON.stringify('mapzen-RCoYPRw')
				},
				'DEBUG': true
			}),
			new webpack.LoaderOptionsPlugin({
				debug: true
			})
		]),
		devServer: {
			contentBase: paths.out,
			publicPath: paths.publicPath,
			port: 10000,
			host: '0.0.0.0',
			historyApiFallback: {
				index: '/checkout/index.html'
			}
		}
	});
};

const productionConfigFactory = (env) => {
	return Object.assign({}, defaultConfig, {
		plugins: defaultPlugins.concat([
			new webpack.DefinePlugin({
				'process.env': {
					NODE_ENV: JSON.stringify('production'),
					MAP_SERVICE_API_KEY: JSON.stringify('mapzen-RCoYPRw')
				},
				'DEBUG': false
			}),
			new webpack.optimize.UglifyJsPlugin({
				minimize: true,
				compress: {
					warnings: false
				},
				mangle: {
					keep_fnames: true
				}
			})
		])
	});
};

if (!NODE_ENV || NODE_ENV === 'development') {
	module.exports = developmentConfigFactory();
} else if (NODE_ENV === 'production') {
	module.exports = productionConfigFactory('production');
} else {
	throw new Error(`NODE_ENV = ${NODE_ENV} has to either development or production`);
}

