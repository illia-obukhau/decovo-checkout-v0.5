#README

Decovo Checkout Frontend V1

##How to lint?
1. lint JS
    ```npm run lint```
2. lint CSS
    ```npm run csslint```
3. fix CSS issues
    ```npm run csscomb```
    
##Before commit with new dependencies
1. npm prune --production
2. npm shrinkwrap
