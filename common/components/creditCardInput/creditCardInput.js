const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');
const classnames = require('classnames');

const ReactMaskedInput = require('react-maskedinput').default;

const cardTypes = require('./creditCardTypes');

require('./creditCardInput.less');

class CreditCardInput extends Component {
	constructor(props, context, updater) {
		super(props, context, updater);

		this.defaultMask = '1111-1111-1111-1111';

		this.state = {
			mask: this.defaultMask
		};
	}

	componentWillMount() {
		const mask = this.getMask(this.props.value);

		this.setState({ mask });
	}

	componentWillReceiveProps(newProps) {
		const mask = this.getMask(newProps.value);

		this.setState({ mask });
	}

	onChange() {
		const { onChange } = this.props;

		const rawValue = this.getRawValue();
		const mask = this.getMask(rawValue);

		this.setState({ mask }, () => {
			const newRawValue = this.getRawValue();

			onChange(newRawValue);
		});
	}

	getRawValue() {
		const maskControl = this.control && this.control.mask;

		return maskControl && maskControl.getRawValue();
	}

	getMask(value) {
		if (value) {
			const cardType = cardTypes.find((cardTypeItem) => {
				return cardTypeItem.beginWith.find((firstDigits) => {
					return value.indexOf(firstDigits) === 0;
				});
			});

			if (cardType) {
				return cardType.mask;
			}
		}

		return this.defaultMask;
	}

	render() {
		const { className, name, placeholder, value, onBlur, disabled, isInvalid } = this.props;
		const { mask } = this.state;
		const creditCardInputClassName = classnames('credit-card-input', className, {
			'credit-card-input--invalid': isInvalid
		});

		return (
			<ReactMaskedInput
				className={creditCardInputClassName}
				ref={(control) => {
					this.control = control;
				}}
				mask={mask}
				name={name}
				type="text"
				placeholder={placeholder}
				value={value}
				onChange={this.onChange.bind(this)}
				onBlur={onBlur}
				disabled={disabled}
			/>
		);
	}
}

CreditCardInput.propTypes = {
	className: PropTypes.string,
	placeholder: PropTypes.string,
	name: PropTypes.string,
	value: PropTypes.string,
	onChange: PropTypes.func.isRequired,
	onBlur: PropTypes.func,
	disabled: PropTypes.bool,
	isInvalid: PropTypes.bool
};

CreditCardInput.defaultProps = {
	className: '',
	name: '',
	value: '',
	onBlur: null,
	disabled: false,
	placeholder: '00000000000000xx'
};

module.exports = CreditCardInput;

