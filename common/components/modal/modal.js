

const React = require('react');
const { PropTypes } = require('prop-types');

const { Component } = React;
const ReactModal = require('react-modal');
const classnames = require('classnames');
const Spinner = require('common/components/spinner');

require('./modal.less');

class Modal extends Component {
	renderCloseButton(onRequestClose) {
		return (
			<button
				className="modal__close-button"
				onClick={onRequestClose}>
				<span className="modal__close-button-icon icon icon-close" />
			</button>
		);
	}

	renderGlobalModal(modalClassNames, overlayClassNames) {
		const { children, isOpen, onRequestClose, isBusy, isCloseButton, transitionTimeout } = this.props;
		const closeButton = isCloseButton && this.renderCloseButton(onRequestClose);

		return (
			<ReactModal
				className={modalClassNames}
				overlayClassName={overlayClassNames}
				isOpen={isOpen}
				closeTimeoutMS={transitionTimeout}
				contentLabel="Modal"
				onRequestClose={onRequestClose}>
				<Spinner isVisible={isBusy}>
					{children}
					{closeButton}
				</Spinner>
			</ReactModal>
		);
	}

	renderLocalModal(modalClassNames, overlayClassNames) {
		const { children, isOpen, onRequestClose, isBusy, isCloseButton } = this.props;
		const closeButton = isCloseButton && this.renderCloseButton(onRequestClose);

		return isOpen && (
			<div className={overlayClassNames} onClick={onRequestClose} role="dialog">
				<div
					className={modalClassNames}
					role="dialog"
					onClick={(event) => {
						event.stopPropagation();
					}}>
					<Spinner isVisible={isBusy}>
						{children}
						{closeButton}
					</Spinner>
				</div>
			</div>
		);
	}

	render() {
		const { className, overlayClassName, isLocal, transitionTimeout } = this.props;
		const modalClassNames = classnames('modal', {
			'modal--animated': transitionTimeout
		}, className);
		const overlayClassNames = classnames('modal__overlay', {
			'modal__overlay--local': isLocal,
			'modal__overlay--animated': transitionTimeout
		}, overlayClassName);

		return isLocal ? this.renderLocalModal(modalClassNames, overlayClassNames) :
			this.renderGlobalModal(modalClassNames, overlayClassNames);
	}
}

Modal.propTypes = {
	children: PropTypes.node,
	isOpen: PropTypes.bool,
	onRequestClose: PropTypes.func,
	className: PropTypes.string,
	overlayClassName: PropTypes.string,
	isBusy: PropTypes.bool,
	isLocal: PropTypes.bool,
	isCloseButton: PropTypes.bool,
	transitionTimeout: PropTypes.number
};

Modal.defaultProps = {
	isBusy: false
};

module.exports = Modal;

