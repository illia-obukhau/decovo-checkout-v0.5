const React = require('react');
const PropTypes = require('prop-types');
const classnames = require('classnames');

const NumericInput = require('common/components/numericInput');
const SelectBox = require('common/components/selectBox');
const SelectBoxOptionModel = require('common/components/selectBox/selectBoxOptionModel');

const { Component } = React;

class AmountInput extends Component {
	constructor(props, context, updater) {
		super(props, context, updater);

		this.onChange = this.onChange.bind(this);
	}

	onChange(newValue) {
		if (newValue !== null) {
			const parsedValue = parseInt(newValue, 10);
			this.props.onChange(parsedValue);
		}
	}

	generateOptions() {
		const maxAmountForSelect = this.props.maxAmountForSelect;
		const options = [];
		for (let index = 1; index < maxAmountForSelect; index++) {
			const stringValue = index.toString();
			options.push(new SelectBoxOptionModel(stringValue, stringValue));
		}
		const boundaryValue = (maxAmountForSelect).toString();
		options.push(new SelectBoxOptionModel(boundaryValue, `${boundaryValue}+`));

		return options;
	}

	renderNumericInput() {
		const { className, value, name } = this.props;

		return (<NumericInput
			name={name}
			className={className}
			value={Number(value)}
			onBlur={this.onChange}
			min={1}
		/>);
	}

	renderSelectBox() {
		const { className, value, name } = this.props;
		const options = this.generateOptions();

		return (<SelectBox
			name={name}
			className={className}
			value={value.toString()}
			onChange={this.onChange}
			options={options}
			defaultOption={options[0]}
		/>);
	}

	render() {
		const { className, value, maxAmountForSelect } = this.props;
		const isBelowBoundary = maxAmountForSelect && (maxAmountForSelect > value);
		const classNames = classnames('amount-input', className);
		const amountInputContainer = isBelowBoundary ?
			this.renderSelectBox() :
			this.renderNumericInput();

		return (
			<div className={classNames}>
				<div className="amount-input__inner-container">
					{amountInputContainer}
				</div>
			</div>
		);
	}
}

AmountInput.propTypes = {
	value: PropTypes.number.isRequired,
	className: PropTypes.string,
	name: PropTypes.string.isRequired,
	maxAmountForSelect: PropTypes.number,
	onChange: PropTypes.func.isRequired
};

module.exports = AmountInput;
