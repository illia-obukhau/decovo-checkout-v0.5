const React = require('react');
const { PropTypes } = require('prop-types');

const { Component } = React;
const classnames = require('classnames');

const SelectBoxOptionModel = require('./selectBoxOptionModel');
const Select = require('react-select').default;

require('./selectBox.less');
require('react-select/dist/react-select.css');

class SelectBox extends Component {
	onChange(model) {
		this.props.onChange(model ? model.value : model);
	}

	getSelectedOption(options, value, defaultOption) {
		if (value) {
			const stringValue = value.toString();

			return options.find(option => option.value.toString() === stringValue) || defaultOption;
		}

		return defaultOption;
	}

	render() {
		const { className, options, placeholder, defaultOption, onFocus, value, disabled, isInvalid, onBlur } = this.props;
		const selectedOption = this.getSelectedOption(options, value, defaultOption);
		const selectBoxClassNames = classnames(className, 'select-box', {
			'select-box--invalid': isInvalid
		});

		return (
			<Select
				className={selectBoxClassNames}
				name={name}
				options={options}
				onFocus={onFocus}
				onBlur={onBlur}
				value={selectedOption.value}
				placeholder={placeholder}
				onChange={this.onChange.bind(this)}
				clearable={false}
				searchable={false}
				disabled={disabled}
			/>
		);
	}
}

SelectBox.propTypes = {
	placeholder: PropTypes.string,
	onFocus: PropTypes.func,
	onChange: PropTypes.func,
	onBlur: PropTypes.func,
	options: PropTypes.arrayOf(PropTypes.instanceOf(SelectBoxOptionModel)).isRequired,
	defaultOption: PropTypes.instanceOf(SelectBoxOptionModel),
	className: PropTypes.string,
	isInvalid: PropTypes.bool,
	value: PropTypes.string,
	disabled: PropTypes.bool
};

SelectBox.defaultProps = {
	defaultOption: new SelectBoxOptionModel('', 'Select...')
};

module.exports = SelectBox;
