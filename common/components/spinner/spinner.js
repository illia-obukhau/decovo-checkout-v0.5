const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');
const window = require('global/window');

require('./spinner.less');

class Spinner extends Component {
	constructor(props, context) {
		super(props, context);
		this.defaultTimeout = 300;

		this.state = {
			isVisible: !this.props.isLoaded
		};

		this.timer = null;
	}

	componentWillReceiveProps(nextProps) {
		window.clearTimeout(this.timer);

		if (nextProps.isLoaded) {
			this.timer = window.setTimeout(() => {
				this.setState({
					isVisible: false
				});
			}, this.defaultTimeout);
		}
	}

	componentWillUnmount() {
		window.clearTimeout(this.timer);
	}

	renderContent() {
		if (this.state.isVisible) {
			return (
				<div className="spinner__loader">
					<img src="checkout/static/images/ring.gif" alt="Loading..." />
				</div>
			);
		}

		return this.props.children;
	}

	render() {
		return (
			<div className="spinner">
				{this.renderContent()}
			</div>
		);
	}
}

Spinner.propTypes = {
	isLoaded: PropTypes.bool.isRequired,
	children: PropTypes.node.isRequired
};

module.exports = Spinner;
