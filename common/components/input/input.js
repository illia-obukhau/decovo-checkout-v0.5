const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');
const classnames = require('classnames');

require('./input.less');

class Input extends Component {
	setFocus() {
		this.input.focus();
	}

	render() {
		const { placeholder, className, onBlur, onFocus, onChange, onClick, value, isInvalid } = this.props;

		const inputClassNames = classnames(className, 'text-input', {
			'text-input--invalid': isInvalid
		});

		return (
			<input
				className={inputClassNames}
				type="text"
				placeholder={placeholder}
				onClick={onClick}
				onChange={onChange}
				onBlur={onBlur}
				onFocus={onFocus}
				value={value}
				ref={(input) => { this.input = input; }}
			/>
		);
	}
}

Input.propTypes = {
	className: PropTypes.string,
	placeholder: PropTypes.string,
	onChange: PropTypes.func,
	onClick: PropTypes.func,
	onFocus: PropTypes.func,
	onBlur: PropTypes.func,
	value: PropTypes.string,
	isInvalid: PropTypes.bool
};

module.exports = Input;

