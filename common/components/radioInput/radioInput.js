const React = require('react');

const { Component } = React;

const classnames = require('classnames');

const { PropTypes } = require('prop-types');
const RadioInputOptionModel = require('./radioInputOptionModel');

require('./radioInput.less');
const { RadioGroup, Radio } = require('react-radio-group');

class RadioInput extends Component {
	constructor(props, context, updater) {
		super(props, context, updater);

		this.onChange = this.onChange.bind(this);
	}

	onChange(newValue) {
		const { options, onChange } = this.props;
		const selectedValue = options.find(option => option.value === newValue);

		onChange(selectedValue);
	}

	render() {
		const { name, options, value, className, inputClassName } = this.props;
		const radioGroupClassName = classnames(className, 'radio-group');

		return (
			<RadioGroup className={radioGroupClassName} onChange={this.onChange} selectedValue={value.value}>
				{
					options.map((option, index) => {
						const isChecked = value.value === option.value;
						const inputClassNames = classnames(inputClassName, 'radio-input', {
							'radio-input--checked': isChecked
						});

						return (
							<label className={inputClassNames} key={`${name}-${index}`}>
								<Radio className="radio-input__input" value={option.value} />
								<span className="radio-input__bullet-container" />
								<span className="radio-input__label">{option.label}</span>
							</label>
						);
					})
				}
			</RadioGroup>
		);
	}
}

RadioInput.propTypes = {
	options: PropTypes.arrayOf(PropTypes.instanceOf(RadioInputOptionModel)),
	name: PropTypes.string,
	className: PropTypes.string,
	inputClassName: PropTypes.string,
	onChange: PropTypes.func,
	value: PropTypes.instanceOf(RadioInputOptionModel)
};

module.exports = RadioInput;
