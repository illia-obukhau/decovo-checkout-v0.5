

class RadioInputOptionModel {
	constructor(value, label) {
		this.value = value;
		this.label = label;
	}
}

module.exports = RadioInputOptionModel;
