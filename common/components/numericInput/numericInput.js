const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');

const classnames = require('classnames');

require('./numericInput.less');

class NumericInput extends Component {
	constructor(props, context, updater) {
		super(props, context, updater);

		this.state = {
			value: ''
		};

		this.onChange = this.onChange.bind(this);
		this.onBlur = this.onBlur.bind(this);
	}

	componentWillMount() {
		this.setState({
			value: this.props.value
		});
	}

	onChange(event) {
		const { onChange } = this.props;
		const newValue = event.target.value;

		if (newValue || newValue === 0) {
			this.setState({
				value: newValue
			}, () => {
				if (onChange) {
					onChange(this.state.value);
				}
			});
		}
	}

	onBlur(event) {
		const { onBlur } = this.props;

		if (onBlur) {
			onBlur(this.state.value);
		}
	}

	render() {
		const { className, name, disabled, min, max, step } = this.props;

		const inputClassName = classnames('numeric-input', className);

		return (
			<input
				className={inputClassName}
				type="number"
				name={name}
				value={this.state.value}
				min={min}
				max={max}
				step={step}
				onChange={this.onChange.bind(this)}
				onBlur={this.onBlur.bind(this)}
				disabled={disabled}
			/>
		);
	}
}

NumericInput.propTypes = {
	className: PropTypes.string,
	name: PropTypes.string,
	value: PropTypes.number,
	onChange: PropTypes.func,
	onBlur: PropTypes.func,
	disabled: PropTypes.bool,
	min: PropTypes.number,
	max: PropTypes.number,
	step: PropTypes.number
};

module.exports = NumericInput;

