const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');
const classnames = require('classnames');

require('./operationLoader.less');

class OperationLoader extends Component {
	renderLoader() {
		const messageClassNames = classnames('operation-loader__message', this.props.messageClass);

		if (this.props.isVisible) {
			return (
				<div className="operation-loader__message-container">
					<span className={messageClassNames}>{this.props.message}</span>
				</div>
			);
		}

		return null;
	}

	render() {
		const { children, isVisible } = this.props;
		const classNames = classnames('operation-loader', {
			'operation-loader--visible': isVisible
		});

		return (
			<div className={classNames}>
				<div className="operation-loader__inner-container">
					<div className="operation-loader__overlay" />
					<div className="operation-loader__content">{children}</div>
					{this.renderLoader()}
				</div>
			</div>
		);
	}
}

OperationLoader.propTypes = {
	message: PropTypes.string,
	messageClass: PropTypes.string,
	isVisible: PropTypes.bool.isRequired,
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.element),
		PropTypes.element
	])
};

module.exports = OperationLoader;
