const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');
const classnames = require('classnames');

require('./panel.less');

class Panel extends Component {
	render() {
		const { children, className } = this.props;
		const panelClassNames = classnames('panel', className);

		return (
			<div className={panelClassNames}>
				<div className="panel__inner-container">
					{children}
				</div>
			</div>
		);
	}
}

Panel.propTypes = {
	children: PropTypes.node.isRequired,
	className: PropTypes.string
};

Panel.defaultProps = {
	className: ''
};

module.exports = Panel;

