const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');
const currencyHelper = require('common/helpers/currencyHelper');
const classnames = require('classnames');

require('./price.less');

class Price extends Component {
	render() {
		const { value, format, className } = this.props;
		const formattedValue = currencyHelper.convertFromCentsToDollars(value, format);
		const priceClassName = classnames('price', className);

		return (
			<span className={priceClassName}>{formattedValue}</span>
		);
	}
}

Price.propTypes = {
	value: PropTypes.number.isRequired,
	format: PropTypes.string.isRequired,
	className: PropTypes.string
};

Price.defaultProps = {
	format: '$0,0.00',
	className: ''
};

module.exports = Price;

