const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');
const classnames = require('classnames');
const phoneFormats = require('common/constants/phoneFormats');
const phoneHelper = require('common/helpers/phoneHelper');

require('./phone.less');

class Phone extends Component {
	render() {
		const { value, format, className } = this.props;
		const phoneClassName = classnames('phone', className);
		const phoneReference = phoneHelper.format(value, phoneFormats.e164);
		const formattedPhoneNumber = phoneHelper.format(value, format);

		return (
			<a className={phoneClassName} href={`tel:${phoneReference}`}>{formattedPhoneNumber}</a>
		);
	}
}

Phone.propTypes = {
	value: PropTypes.string.isRequired,
	format: PropTypes.oneOf([phoneFormats.international, phoneFormats.national, phoneFormats.e164, phoneFormats.dottedNational]),
	className: PropTypes.string
};

Phone.defaultProps = {
	format: phoneFormats.national,
	className: ''
};

module.exports = Phone;

