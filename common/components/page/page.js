const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');
const classnames = require('classnames');
require('./page.less');
const Spinner = require('common/components/spinner');

class Page extends Component {
	renderTitleContainer(title) {
		let titleContainer = null;
		if (title) {
			titleContainer = (
				<div className="page__title-container">
					<h2 className="page__title">{title}</h2>
				</div>
			);
		}

		return titleContainer;
	}

	render() {
		const { className, children, title, isLoaded } = this.props;
		const pageClassNames = classnames('page', className);
		const titleContainer = this.renderTitleContainer(title);

		return (
			<div className={pageClassNames}>
				<div className="page__inner-container layout__container">
					<Spinner isLoaded={isLoaded}>
						{titleContainer}

						<div className="page__content">
							{children}
						</div>
					</Spinner>
				</div>
			</div>
		);
	}
}

Page.propTypes = {
	children: PropTypes.node.isRequired,
	title: PropTypes.node,
	className: PropTypes.string,
	isLoaded: PropTypes.bool
};

Page.defaultProps = {
	className: null,
	title: null,
	isLoaded: true
};

module.exports = Page;

