

const React = require('react');

const { Component } = React;
const { PropTypes } = require('prop-types');

const ReactMaskedInput = require('react-maskedinput').default;
const classnames = require('classnames');

require('./maskedInput.less');

class MaskedInput extends Component {
	onChange() {
		return this.props.onChange(this.getRawValue());
	}

	onBlur() {
		return this.props.onBlur(this.getRawValue());
	}

	getRawValue() {
		const mask = this.control && this.control.mask;

		return mask.getRawValue().replace(new RegExp('_', 'g'), '');
	}

	render() {
		const { className, type, mask, name, placeholder, value, disabled, isInvalid } = this.props;
		const inputClassNames = classnames(className, 'masked-text-input', {
			'masked-text-input--invalid': isInvalid
		});

		return (
			<ReactMaskedInput
				className={inputClassNames}
				ref={(input) => { this.control = input; }}
				mask={mask}
				name={name}
				type={type}
				placeholder={placeholder}
				value={value}
				onChange={this.onChange.bind(this)}
				onBlur={this.onBlur.bind(this)}
				disabled={disabled}
			/>
		);
	}
}

MaskedInput.propTypes = {
	className: PropTypes.string,
	type: PropTypes.string,
	name: PropTypes.string,
	mask: PropTypes.string,
	placeholder: PropTypes.string,
	value: PropTypes.string,
	onChange: PropTypes.func,
	onBlur: PropTypes.func,
	disabled: PropTypes.bool,
	isInvalid: PropTypes.bool
};

MaskedInput.defaultProps = {
	type: 'text'
};

module.exports = MaskedInput;

