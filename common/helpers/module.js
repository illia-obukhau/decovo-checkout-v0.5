class Module {
	constructor(...childModules) {
		this.childModules = childModules;
		this.isConfigured = false;
		this.configurators = [];
	}

	configure(callback) {
		this.configurators.push(callback);

		return this;
	}

	run() {
		if (!this.isConfigured && Array.isArray(this.childModules)) {
			this.childModules.forEach(mod => mod.run());
		}

		this.configurators.forEach(configure => configure());
	}
}

module.exports = Module;
