const numeral = require('numeral');

const round = function (value, decimals) {
	const number = Number(`${value}e${decimals}`);
	const roundedValue = Math.round(number);

	return Number(`${roundedValue}e-${decimals}`);
};

module.exports.round = round;

module.exports.convertFromDollarsToCents = function (dollars) {
	const cents = numeral(dollars).multiply(100).value();

	return round(cents, 0);
};

module.exports.convertFromCentsToDollars = function (cents, format) {
	const dollars = numeral(cents).divide(100).value();
	const roundedDollars = round(dollars, 2);

	return numeral(roundedDollars).format(format);
};

module.exports.parseDollars = function (numericString) {
	return numeral().unformat(numericString);
};

module.exports.calculateTaxPrice = function (cents, taxPercent) {
	const tax = numeral(taxPercent).divide(100).value();

	return numeral(cents).multiply(tax).value();
};
