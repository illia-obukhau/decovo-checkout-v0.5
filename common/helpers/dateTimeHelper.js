const moment = require('moment');
const dateTimeFormats = require('common/constants/dateTimeFormats');

module.exports.getDateAfterToday = function (daysCount) {
	const today = moment().utcOffset(0).startOf('day');

	return today.clone().add(daysCount, 'day').format(dateTimeFormats.dateFormat);
};

module.exports.getNextDayFor = function (date) {
	const targetDay = moment.utc(date, dateTimeFormats.dateFormat);

	return targetDay.clone().add(1, 'day').format(dateTimeFormats.dateFormat);
};

module.exports.isBefore = function (date1, date2, format1, format2) {
	const date1Format = format1 || dateTimeFormats.dateTimeFormat;
	const date2Format = format2 || dateTimeFormats.dateTimeFormat;

	return moment.utc(date1, date1Format).isBefore(moment.utc(date2, date2Format));
};

module.exports.isSame = function (date1, date2, format1, format2) {
	const date1Format = format1 || dateTimeFormats.dateTimeFormat;
	const date2Format = format2 || dateTimeFormats.dateTimeFormat;

	return moment.utc(date1, date1Format).isSame(moment.utc(date2, date2Format));
};

module.exports.getCurrentDateTime = function () {
	return moment.utc().format(dateTimeFormats.dateTimeFormat);
};

module.exports.getCurrentDate = function () {
	return moment().utcOffset(0).startOf('day').format(dateTimeFormats.dateFormat);
};

module.exports.formatWithTimeZone = function (date, timeZone, format1, format2) {
	const toFormat = format2 || format1;
	const fromFormat = format2 && format1;

	return moment.utc(date, fromFormat).utcOffset(timeZone).format(toFormat);
};

module.exports.format = function (date, format1, format2) {
	const toFormat = format2 || format1;
	const fromFormat = format2 && format1;

	return moment.utc(date, fromFormat).format(toFormat);
};
