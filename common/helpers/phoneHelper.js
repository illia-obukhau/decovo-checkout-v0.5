const phoneFormatter = require('phone-formatter');
const phoneFormats = require('common/constants/phoneFormats');
const countryPhoneCodes = require('common/constants/countryPhoneCodes');

class PhoneHelper {
	convertToRawNational(phoneString) {
		const formattedPhone = phoneFormatter.format(phoneString, phoneFormats.e164);

		return formattedPhone.replace(countryPhoneCodes.US, '');
	}

	format(phoneString, format) {
		const primaryFormattedPhone = phoneFormatter.format(phoneString, format);
		switch (format) {
			case phoneFormats.international:
			case phoneFormats.e164:
				return countryPhoneCodes.US + primaryFormattedPhone;
			default:
				return primaryFormattedPhone;
		}
	}
}

module.exports = new PhoneHelper();
