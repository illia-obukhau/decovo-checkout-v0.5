const URLSearchParams = require('url-search-params');
const fetchIt = require('fetch-it');

const defaults = {
	headers: {
		'Accept': 'application/json',
		'Content-Type': 'application/json'
	},
	credentials: 'same-origin'
};

const streamInterceptor = {
	response(res) {
		return res.json()
			.catch(() => null)
			.then(json => ({ res, json }));
	}
};

function buildUrl(urlString, queryParams) {
	const urlSearchParams = new (URLSearchParams)();

	Object.keys(queryParams).forEach((parameterName) => {
		const parameter = queryParams[parameterName];
		if (Array.isArray(parameter)) {
			parameter.forEach((item, index) => urlSearchParams.append(parameterName, parameter[index]));
		} else {
			urlSearchParams.set(parameterName, parameter);
		}
	});

	return `${urlString}?${urlSearchParams}`;
}

function request(url, config) {
	const options = Object.assign({}, defaults, config);

	options.body = options.body && JSON.stringify(options.body);

	return fetchIt.fetch(url, options);
}

function get(url, queryParams, config) {
	const options = Object.assign({
		method: 'GET'
	}, config);

	const composedUrl = queryParams ? buildUrl(url, queryParams) : url;

	return request(composedUrl, options);
}

function post(url, body, config) {
	const options = Object.assign({
		method: 'POST',
		body
	}, config);

	return request(url, options);
}

function put(url, body, config) {
	const options = Object.assign({
		method: 'PUT',
		body
	}, config);

	return request(url, options);
}

function remove(url, body, config) {
	const options = Object.assign({
		method: 'DELETE',
		body
	}, config);

	return request(url, options);
}

fetchIt.addMiddlewares([streamInterceptor]);

module.exports = {
	request,
	get,
	post,
	put,
	delete: remove,
	addMiddlewares: fetchIt.addMiddlewares.bind(fetchIt)
};
