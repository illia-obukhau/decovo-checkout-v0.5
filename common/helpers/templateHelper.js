const format = require('string-template');

class TemplateHelper {
	format(templateString, paramDictionary) {
		return format(templateString, paramDictionary);
	}
}

module.exports = new TemplateHelper();
