const ClientValidationError = require('../customErrors/clientValidationError');

const pending = (state, action) => {
	return state.merge({
		isBusy: true
	});
};

const success = (state, action) => {
	const newState = {
		isSynchronized: true,
		isBusy: false,
		isSynchronizationError: false,
		validationError: null
	};

	if (Array.isArray(action.payload)) {
		newState.items = action.payload;
	} else {
		newState.model = action.payload;
	}

	return state.merge(newState);
};

const error = (state, action) => {
	const validationError = action.payload;

	return state.merge({
		isBusy: false,
		isSynchronizationError: true,
		validationError: validationError instanceof ClientValidationError ? validationError : null
	});
};

const reset = (state, action) => {
	return state.merge({
		model: null,
		isBusy: false,
		isSynchronized: false,
		isSynchronizationError: false,
		validationError: null
	});
};

const set = (state, action) => {
	if (Array.isArray(action.payload)) {
		return state.merge({
			items: action.payload
		});
	}

	return state.merge({
		model: action.payload
	});
};

module.exports = {
	pending,
	success,
	error,
	reset,
	set
};
