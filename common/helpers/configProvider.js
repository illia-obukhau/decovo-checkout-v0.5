const merge = require('lodash/merge');
const cloneDeep = require('lodash/cloneDeep');
const isString = require('lodash/isString');
const get = require('lodash/get');

class ConfigProvider {
	constructor(defaultConfig, envConfig) {
		this._defaultConfig = defaultConfig;
		this._envConfig = envConfig;
	}

	_getRawConfig() {
		if (this._config === undefined) {
			this._config = merge(cloneDeep(this._defaultConfig), cloneDeep(this._envConfig));
		}

		return this._config;
	}

	get(path) {
		if (!isString(path)) {
			throw new Error('Calling get only allowed with string path argument');
		}
		const rawConfig = this._getRawConfig();
		const value = get(rawConfig, path);
		if (value === undefined) {
			throw new Error(`Configuration property "${path}" is not defined`);
		}

		return cloneDeep(value);
	}
}

module.exports = ConfigProvider;
