const ConfigProvider = require('./configProvider');

const getEnvConfig = () => {
	if (process.env.NODE_ENV === 'development') {
		return require('config/development'); // eslint-disable-line global-require
	}
	if (process.env.NODE_ENV === 'production') {
		return require('config/production'); // eslint-disable-line global-require
	}

	throw new Error(`Missing config for ${process.env.HOSTNAME} env.`);
};

const envConfig = getEnvConfig();
const defaultConfig = require('config/default'); // eslint-disable-line global-require

module.exports = new ConfigProvider(defaultConfig, envConfig);
