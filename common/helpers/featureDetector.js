
const window = require('global/window');

module.exports.isMobile = function () {
	const media = window.matchMedia && window.matchMedia('(max-width: 767px)');
	const isTouch = 'ontouchstart' in window;

	return media && media.matches && isTouch;
};
