const CustomError = require('./customError');

class BadGatewayError extends CustomError {
	constructor() {
		super('Server unavailable', 'Server unavailable. Please, try again later!');
	}
}

module.exports = BadGatewayError;
