const CustomError = require('./customError');

class NotFoundError extends CustomError {
}

module.exports = NotFoundError;
