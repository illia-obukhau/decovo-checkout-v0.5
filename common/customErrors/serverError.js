const CustomError = require('./customError');

class ServerError extends CustomError {
}

module.exports = ServerError;
