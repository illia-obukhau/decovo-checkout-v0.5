const CustomError = require('./customError');

class NotAuthorizedError extends CustomError {
}

module.exports = NotAuthorizedError;
