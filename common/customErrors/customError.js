function CustomError(message, clientMessage) {
	if (Error.captureStackTrace) {
		Error.captureStackTrace(this, this.constructor);
	}

	this.name = this.constructor.name;
	this.message = message;
	this.clientMessage = clientMessage;
}

CustomError.prototype = Object.create(Error.prototype);
CustomError.prototype.constructor = CustomError;
CustomError.prototype.super = Error.prototype;

module.exports = CustomError;
