const CustomError = require('./customError');

class ValidationError extends CustomError {
	constructor(message, errors) {
		super(message);

		this.errors = errors;
	}
}

module.exports = ValidationError;
