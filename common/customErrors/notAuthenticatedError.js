const CustomError = require('./customError');

class NotAuthenticatedError extends CustomError {
}

module.exports = NotAuthenticatedError;
