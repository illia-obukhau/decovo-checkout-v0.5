const CustomError = require('./customError');

class PaymentError extends CustomError {
}

module.exports = PaymentError;
