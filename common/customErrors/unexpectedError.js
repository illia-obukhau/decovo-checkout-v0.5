const CustomError = require('./customError');

class UnexpectedError extends CustomError {
	constructor(message, payload) {
		super(message);

		this.payload = payload;
	}
}

module.exports = UnexpectedError;
