const ValidationError = require('./validationError');

class ClientValidationError extends ValidationError {
}

module.exports = ClientValidationError;
