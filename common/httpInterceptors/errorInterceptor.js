const BadGatewayError = require('../customErrors/badGatewayError');
const NotAuthorizedError = require('../customErrors/notAuthorizedError');
const NotAuthenticatedError = require('../customErrors/notAuthenticatedError');
const ServerError = require('../customErrors/serverError');
const ClientValidationError = require('../customErrors/clientValidationError');
const PaymentError = require('../customErrors/paymentError');

module.exports = {

	response({ res, json }) {
		// TODO: In case of not using errorInterceptor the return object will be {res. json}
		if (res.ok) {
			return json;
		}

		const { message, errors, clientMessage } = json || {};

		switch (res.status) {
			case 401:
				throw new NotAuthenticatedError(message, clientMessage);
			case 402:
				throw new PaymentError(message, clientMessage);
			case 403:
				throw new NotAuthorizedError(message, clientMessage);
			case 500:
				throw new ServerError(message, clientMessage);
			case 502:
				throw new BadGatewayError();
			case 422:
				throw new ClientValidationError(message, errors, clientMessage);
			case 424:
				throw new ServerError(message, clientMessage);
			case 404:
				throw new ServerError(message, clientMessage);
			default:
				throw new ServerError(message, clientMessage);
		}
	}

};
